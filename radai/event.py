from .result import Results, ResultsBatch
from pandas import DataFrame, Series, concat
import numpy as np


class Event(object):
    def __init__(self):
        """A collection of time-correlated alarming algorithm results"""
        self._r = DataFrame()
        self.uuid = None
        self._active = True

    @property
    def active(self):
        return self._active

    def duration(self, source_id=None):
        if len(self) > 0 and source_id is None:
            return self.timestamp_stop.max() - self.timestamp_start.min()
        elif source_id is not None:
            msk = self._r.is_alarm.values & (self._r.source_id.values == source_id)
            return (
                self._r.loc[msk, "timestamp_stop"].max()
                - self._r.loc[msk, "timestamp_start"].min()
            )
        else:
            return 0.0

    @property
    def results(self):
        return self._r.copy()

    @property
    def timestamp_start(self):
        if len(self) > 0:
            return self._r.timestamp_start.min()
        else:
            return None

    @property
    def timestamp_stop(self):
        if len(self) > 0:
            return self._r.timestamp_stop.max()
        else:
            return None

    @property
    def timestamp_alarm(self):
        """Get the timestamp at the maximum alarm_metric"""
        if len(self) == 0:
            return None
        idx = self._r.alarm_metric.idxmax()
        v = self._r.loc[idx]
        if v.timestamp_alarm is not None:
            val = v.timestamp_alarm
        else:
            val = v.timestamp_start
        return val

    def append(self, res: Results):
        if not self.active:
            # can't append to an inactive event
            raise BufferError("Event is not active.")
        elif (len(self) == 0) and (not res.contains_alarm()):
            # don't start appending results until an alarm
            pass
        else:
            # append into a dataframe, extend integer index
            self._r = concat([self._r, res.to_dataframe()], ignore_index=True)

    def clean(self, force=False):
        """Clean trailing non-alarms
        force will force this to happen even if active
        """
        if not self.active or force:
            # get the time of the last alarm
            tc = self._r.loc[self._r.is_alarm, "timestamp_start"].max()
            # keep rows with times before that
            self._r = self._r.loc[self._r.timestamp_start <= tc]
        else:
            print("Can't clean, Event is still active")

    def summary(self):
        if len(self) > 0:
            dic = {
                "uuid": self.uuid,
                "timestamp_start": self.timestamp_start,
                "timestamp_stop": self.timestamp_stop,
                "timestamp_alarm": self.timestamp_alarm,
                # "top_3_id": tn.source_id.values,
                # "top_3_metrics": np.around(tn.alarm_metric.values, 2)
            }
            tn = self.top_n(3)
            for idx, (idy, row) in enumerate(tn.iterrows()):
                dic[f"source_id_{idx}"] = row.source_id
                dic[f"alarm_metric_{idx}"] = np.around(row.alarm_metric, 3)
                dic[f"duration_{idx}"] = self.duration(row.source_id)

            summary = Series(dic)
            return summary
        else:
            return None

    def top_n(self, n=1, is_alarm=True) -> DataFrame:
        """Get the top n alarming source-types
        Returns details about the row with the largest alarm_metric for n source-types
        If fewer than n source-type alarms the DataFrame will be padded with nans
        """
        # Get the strongest alarm of upto n classes
        if is_alarm:
            mask = self._r.is_alarm
        else:
            mask = np.ones(len(self._r)).astype(bool)
        tn_idx = self._r.loc[mask].groupby("source_id")["alarm_metric"].idxmax()
        tn = self._r.loc[tn_idx].sort_values("alarm_metric", ascending=False)
        tn = tn.iloc[:n]
        # if too short than requested pad with nans
        if len(tn) < n:
            tn = concat([tn, DataFrame(index=np.arange(n - len(tn)))])
        return tn

    def to_string(self, header=False):
        """Write summary of the Event to a string"""
        df = DataFrame(self.summary()).T
        return df.to_csv(
            header=header,
            float_format="%.3f",
        )

    def __repr__(self) -> str:
        if len(self) > 0:
            return self.summary().__repr__()
        else:
            return "empty"

    def __len__(self):
        return len(self._r)


class ResultsAggregator(object):
    """Algorithm results aggregation"""

    def __init__(self, cool_time=5.0, max_duration=30.0):
        """Initialize an algorithm results aggregator

        Parameters
        ----------
        cool_time : float
            max time between alarms that will be considered
            as from the same event
        max_duration : float
            max duration an event can go on for
        """
        self.cool_time = cool_time
        self.max_duration = max_duration
        self.inactive_events = []
        self.active_event = Event()

    def append(self, res: Results):
        """Process a results object through aggregation

        Parameters
        ----------
        res : Results
            algorithm results to be aggregated into events
        """
        # event knows not to keep anything until a first alarm
        self.active_event.append(res)
        # check if the event is still active
        self._update_active_event_status()

        self.move_to_inactive()

    def move_to_inactive(self):
        # if the event is no longer active,
        # clean, add to history, make a new empty event
        if not self.active_event.active:
            self.active_event.clean()
            self.inactive_events.append(self.active_event)
            self.active_event = Event()
        # othwerise all good.
        else:
            pass

    def append_results_batch(self, rb: ResultsBatch):
        """Process an entire batch of Results objects"""
        # Slow way
        # for res in rb:
        #     self.append(res)
        # return
        # Faster way
        rbdf = rb.to_dataframe()
        alarm_time_mask = rbdf.groupby("timestamp_start").is_alarm.any()
        event_start = None
        lstalarm = None
        if alarm_time_mask.sum() == 0:
            return

        # loop over alarm start and stops
        # identify events, create entire event at once
        for astart, astop in zip(
            alarm_time_mask.index.values[alarm_time_mask],
            rbdf.timestamp_stop.unique()[alarm_time_mask],
        ):
            if event_start is None:
                event_start = astart
                lstalarm = astop

            # use start here instead of stop like in _update_active_event_status
            # due to using alarms here, whereas that looks at the end of
            # a non-alarming results
            event_cooled = (astart - lstalarm) > self.cool_time
            event_timed_out = (lstalarm - event_start) > self.max_duration
            if event_cooled or event_timed_out:
                # grab results rows, add to event, add to history
                msk = (rbdf.timestamp_start >= event_start) & (
                    rbdf.timestamp_stop <= (lstalarm)
                )
                # self.active_event._r = rbdf.loc[msk].copy()
                self.active_event._r = concat(
                    [self.active_event._r, rbdf.loc[msk]], ignore_index=True
                )
                self.active_event._active = False
                self.move_to_inactive()
                # update helpers for next event window
                event_start = astart
                lstalarm = astop
            else:
                # event continues
                lstalarm = astop

        # add remaining alarms to active_event and leave active
        msk = (rbdf.timestamp_start >= event_start) & (
            rbdf.timestamp_stop <= (lstalarm)
        )
        self.active_event._r = concat(
            [self.active_event._r, rbdf.loc[msk]], ignore_index=True
        )

    def close(self):
        if len(self.active_event) > 0:
            self.active_event.clean(force=True)
            self.inactive_events.append(self.active_event)
            self.active_event = Event()

    def summary(self) -> DataFrame:
        summary = DataFrame([e.summary() for e in self.inactive_events])
        if len(self.active_event) > 0:
            summary = concat([summary, DataFrame(self.active_event.summary())])
        return summary

    def to_csv(self, outfn: str, n: int):
        """Write data to a csv file
        n is the top number of alarms to include for each event
        """
        assert outfn[-3:].lower() == "csv"
        summary = self.summary()
        summary.to_csv(outfn, header=True, float_format="%.3f")

    def _update_active_event_status(self):
        """Check whether the active_event is still active"""
        # event is active if less than max duration
        active = self.active_event.duration() <= self.max_duration

        if len(self.active_event) > 0.0:
            # check if cooldown is exceeded
            lstalarm = self.active_event._r.loc[
                self.active_event._r.is_alarm, "timestamp_stop"
            ].max()
            active &= (
                self.active_event.timestamp_stop.max() - lstalarm
            ) <= self.cool_time

        # set the event state
        self.active_event._active = active

    def __repr__(self) -> str:
        repr = "Active event: {} \n".format(len(self.active_event) > 0)
        repr += "Inactive events: {}".format(len(self.inactive_events))
        return repr
