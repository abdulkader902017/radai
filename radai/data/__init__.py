"""radai.data: Data sets, schemas and manipulation tools for spectrum-like rad data.

Notes
-----
Rad data is stored in numpy arrays (typically float32) with the following dimensions:
    * 1D (e, ) -> single energy spectrum
    * 2D (t, e) -> time sequence energy spectra or waterfall
    * 3D (n, t, e) -> batch of waterfalls
Source and label nomenclature is as follows:
    * source_id refers to the raw integer source ID in the data set
    * source_name refers to the raw source name in the data set
    * label refers to the label string name
    * tier refers to the level of labeling for sources which can group source_id's.
      These tiers are:
        * 0/"source_id"
        * 1/"isotope"
        * 2/"category"
"""

from .core import RadaiDataSetError, RadaiDataSetWarning
from .core.data_set import DataSet
from .listmode_h5.data_set import ListmodeH5
from .listmode_h5.snr_labeler import ListmodeH5SNRLabeler

__all__ = [
    "RadaiDataSetError",
    "RadaiDataSetWarning",
    "DataSet",
    "ListmodeH5",
    "ListmodeH5SNRLabeler",
]
