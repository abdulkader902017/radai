"""hdf5 file data tree validation tools."""
from abc import ABC, abstractmethod
from typing import Callable
import numpy as np
import h5py


class H5SchemaValidationError(Exception):
    """Exception for an invlaid h5file validation"""


class H5SchemaAttributeError(H5SchemaValidationError):
    """Exception for invalid h5 attribute"""


class H5SchemaDataSetError(H5SchemaValidationError):
    """Exception for invalid h5 data set"""


class H5SchemaShareDimError(H5SchemaValidationError):
    """Exception for invalid share dimension."""


class SharedDim:

    __slots__ = ("name", "length")

    def __init__(
        self,
        name: str,
    ):
        self.name = str(name)
        self.length = None

    def check_length(self, length: int) -> None:
        if self.length is None:
            self.length = int(length)
        else:
            if self.length != length:
                raise H5SchemaShareDimError(
                    f"Length mismatch {self.length} vs {length} for dim: {self.name}"
                )


class SharedDims:
    def __init__(self):
        self._dims = {}

    def register(self, name: str, dim: SharedDim) -> None:
        if not isinstance(dim, SharedDim):
            raise H5SchemaShareDimError(f"Dim must be a SharedDim not a {type(dim)}")
        self._dims[str(name)] = dim

    def check_length(self, name: str, length: int) -> None:
        if name not in self._dims:
            raise H5SchemaShareDimError(
                f"Unknown SharedDim {name}. Options are: {self._dims.keys()}"
            )
        self._dims[name].check_length(length)

    # def check_dims(self, name: str, schema: tuple, values: tuple):
    #     if schema is None:
    #         return
    #     assert len(schema) == len(values)
    #     for idx, (dim, val) in enumerate(zip(schema, values)):
    #         if dim is None:
    #             pass
    #         elif isinstance(dim, int):
    #             assert dim == val
    #         elif isinstance(dim, SharedDim):
    #             dim.check_length(val)
    #         else:
    #             raise ValueError(f"Invalid shape schema dim: {dim}")


def check_scalar(value, type_):
    if isinstance(value, type_):
        return True
    elif hasattr(value, "dtype"):
        dtype = np.dtype(type_)
        value_dtype = value.dtype
        if np.issubdtype(value_dtype, dtype):
            return True
    return False


class _Base(ABC):

    __slots__ = ("name", "required", "note")

    def __init__(
        self,
        name: str,
        required: bool = False,
        note: str = None,
    ) -> None:
        self.name = str(name)
        self.required = bool(required)
        self.note = "" if note is None else str(note)

    @abstractmethod
    def validate(self, item) -> None:
        """Validate h5file item"""


class Attribute(_Base):

    __slots__ = (
        "type",
        "list_item_type",
    )

    def __init__(
        self,
        name: str,
        type_,
        list_item_type=None,
        required: bool = False,
        note: str = None,
    ) -> None:
        super().__init__(
            name=name,
            required=required,
            note=note,
        )
        self.type = type_
        self.list_item_type = list_item_type

    def validate(self, item) -> None:
        if self.type is list:
            if isinstance(item, (list, np.ndarray)):
                for _item in item:
                    if not check_scalar(_item, self.list_item_type):
                        raise H5SchemaAttributeError(
                            f"Atttribute list ({self.name}) contains an item "
                            "which is not the correct list item type "
                            f"({self.list_item_type}): {_item} ({type(_item)})"
                        )
            else:
                raise H5SchemaAttributeError(
                    f"Atttribute ({self.name}) is not a list: " f"{item} ({type(item)})"
                )
        else:
            if not check_scalar(item, self.type):
                raise H5SchemaAttributeError(
                    f"Atttribute ({self.name}) is not a {self.type}: "
                    f"{item} ({type(item)})"
                )


class _ItemWithAttributes(_Base):

    __slots__ = ("attributes",)

    def __init__(
        self,
        name: str,
        attributes: list = None,
        required: bool = False,
        note: str = None,
    ) -> None:
        super().__init__(
            name=name,
            required=required,
            note=note,
        )
        self.attributes = []
        if attributes is not None:
            for a in attributes:
                assert isinstance(a, Attribute)
                self.attributes.append(a)

    def validate(self, item) -> None:
        for attribute in self.attributes:
            if attribute.name not in item.attrs:
                if attribute.required:
                    raise H5SchemaAttributeError(
                        f"Required attribute ({attribute.name}) not found in {item}"
                    )
            else:
                attribute.validate(item.attrs[attribute.name])


class DataSet(_ItemWithAttributes):

    __slots__ = (
        "shape",
        "dtype",
    )

    def __init__(
        self,
        name: str,
        dtype: str,
        attributes: list = None,
        shape: tuple = None,
        required: bool = False,
        note: str = None,
    ) -> None:
        super().__init__(
            name=name,
            attributes=attributes,
            required=required,
            note=note,
        )
        self.shape = None
        if shape is not None:
            assert isinstance(shape, tuple)
            for dim in shape:
                if (dim is not None) and not isinstance(dim, (int, str)):
                    raise TypeError(
                        f"DataSet({self.name}) shape values must be None, int "
                        "or str not: {shape}"
                    )
            self.shape = shape
        self.dtype = str(np.dtype(dtype))

    def validate(self, item, shared_dims: SharedDims = None, **kwargs) -> None:
        super().validate(item)
        if not isinstance(item, h5py.Dataset):
            raise H5SchemaValidationError(f"Expected h5py.Dataset not: {type(item)}")
        if self.dtype != str(item.dtype):
            raise H5SchemaDataSetError(
                f"DataSet({self.name}) has the wrong dtype. "
                f"Expected: {self.dtype} vs Found: {item.dtype}"
            )
        if self.shape is not None:
            if len(self.shape) != len(item.shape):
                raise H5SchemaDataSetError(
                    f"DataSet({self.name}) shapes have different len. "
                    f"Expected: {self.shape} vs Found: {item.shape}"
                )
            for schema_dim, found_dim in zip(self.shape, item.shape):
                if isinstance(schema_dim, str):
                    if shared_dims is None:
                        raise H5SchemaDataSetError(
                            f"DataSet({self.name} has str dim {schema_dim} but "
                            "no shared dims provided"
                        )
                    shared_dims.check_length(schema_dim, found_dim)
                elif isinstance(schema_dim, int):
                    if schema_dim != found_dim:
                        raise H5SchemaDataSetError(
                            f"DataSet({self.name}) has different length. "
                            f"Expected: {schema_dim} vs Found: {found_dim}"
                        )
                # Otherwise skip this dimension since it must be None


class IterGroup(_ItemWithAttributes):

    __slots__ = ("item_factory",)

    def __init__(
        self,
        name: str,
        item_factory: Callable,
        attributes: list = None,
        required: bool = False,
        note: str = None,
    ) -> None:
        super().__init__(name=name, attributes=attributes, required=required, note=note)
        self.item_factory = item_factory

    def validate(
        self, item, shared_dims: SharedDims = None, deep: bool = False
    ) -> None:
        super().validate(item)
        for i, name in enumerate(item.keys()):
            if not deep and i > 0:
                pass
            subitem = self.item_factory(i)
            if subitem.name != name:
                raise H5SchemaValidationError(
                    f"{self.__class__}({self.name} has non incremental increasing "
                    f"sub items. Expected: {subitem.name} vs Found: {name}"
                )
            subitem.validate(item[name], shared_dims=shared_dims, deep=deep)


class Group(_ItemWithAttributes):

    __slots__ = ("items", "shared_dims")

    def __init__(
        self,
        name: str,
        items: list,
        attributes: list = None,
        shared_dims: SharedDims = None,
        required: bool = False,
        note: str = None,
    ) -> None:
        super().__init__(
            name=name,
            attributes=attributes,
            required=required,
            note=note,
        )
        self.items = []
        if items is not None:
            for i in items:
                if not isinstance(i, (self.__class__, IterGroup, DataSet)):
                    raise H5SchemaValidationError(
                        f"Group({self.name}) item must be {self.__class__}, "
                        "IterGroup or DataSet"
                    )
                self.items.append(i)
        self.shared_dims = None
        if shared_dims is not None:
            if not isinstance(shared_dims, SharedDims):
                raise H5SchemaValidationError(
                    f"Group({self.name}) SharedDims is the incorrection type: "
                    f"{type(shared_dims)}"
                )
            self.shared_dims = shared_dims

    def validate(
        self, item, shared_dims: SharedDims = None, deep: bool = False
    ) -> None:
        super().validate(item)
        if not isinstance(item, h5py.Group):
            raise H5SchemaValidationError(f"Expected h5py.Group not: {type(item)}")
        if (shared_dims is not None) and (self.shared_dims is not None):
            raise NotImplementedError("Nested SharedDims are not currently supported.")
        shared_dims = shared_dims if self.shared_dims is None else self.shared_dims
        for subitem in self.items:
            if subitem.name not in item.keys():
                if subitem.required:
                    raise H5SchemaAttributeError(
                        f"Item ({subitem.name}) not found in {item}"
                    )
            else:
                subitem.validate(item[subitem.name], shared_dims=shared_dims, deep=deep)


class Root(Group):
    def __init__(
        self,
        items: list,
        attributes: list = None,
        note: str = None,
    ) -> None:
        super().__init__(
            name="root", items=items, attributes=attributes, required=True, note=note
        )

    def validate(self, item, deep: bool = False) -> None:
        if isinstance(item, str):
            with h5py.File(item, "r") as h5file:
                super().validate(h5file, deep=deep)
        else:
            super().validate(item, deep=deep)
