"""Spectrum, Waterfall and Batch label data structures."""
import abc
from typing import Union
import numpy as np
import pandas as pd
from .tools import RadaiDataSetError


class _Labels(abc.ABC):

    __slots__ = "_names", "_booleans", "_scalars"

    def __init__(
        self,
        names: list = None,
        booleans: np.ndarray = None,
        scalars: np.ndarray = None,
    ) -> None:
        self._names = None
        self._booleans = None
        self._scalars = None
        if names is not None:
            self.names = names
        if booleans is not None:
            self.booleans = booleans
        if scalars is not None:
            self.scalars = scalars

    @property
    def num_labels(self) -> int:
        for x in self._names, self.booleans, self.scalars:
            if x is not None:
                return x.shape[-1]
        return 0

    def _check_valid_num_labels(self, arr) -> bool:
        # If there is data make sure the length matches
        if self.num_labels > 0 and arr.shape[-1] != self.num_labels:
            raise RadaiDataSetError(
                f"Cannot assign array of shape {arr.shape} to labels of "
                f"length {self.num_labels}"
            )

    @property
    def names(self) -> np.ndarray:
        """List of source names"""
        if (self._names is None) and (self.num_labels > 0):
            return np.array(
                ["source{i}" for i in range(self.num_labels)], dtype=np.unicode_
            )
        return self._names

    @names.setter
    def names(self, arr: np.ndarray) -> None:
        arr = np.asarray(arr, dtype=np.unicode_)
        assert arr.ndim == 1
        self._check_valid_num_labels(arr)
        self._names = arr

    @property
    def booleans(self) -> np.ndarray:
        """Array of booleans representing the presence of a source."""
        return self._booleans

    @booleans.setter
    def booleans(self, arr: np.ndarray) -> None:
        """Set and validate the source binary labels."""
        arr = np.asarray(arr, dtype=np.bool_)
        self._check_valid_num_labels(arr)
        self._booleans = arr

    @property
    def scalars(self) -> np.ndarray:
        """Array of int or float representing the source weight."""
        return self._scalars

    @scalars.setter
    def scalars(self, arr: np.ndarray) -> None:
        """Set and validate the source scalar values."""
        arr = np.asfarray(arr)
        self._check_valid_num_labels(arr)
        self._scalars = arr

    def is_empty(self) -> bool:
        """Are no source arrays available?"""
        return self.num_labels == 0


class SpectrumLabels(_Labels):
    """Labels for single spectrum [E, ] including background.

    The dimension references are:
        E = energy
        L = labels

    Attributes
    ----------
    names : np.ndarray
        1D array of names [L, ]
    booleans : np.ndarray
        1D array of binary labels [L, ]
    scalars : np.ndarray
        1D array of scalar values [L, ]
    """

    @_Labels.booleans.setter
    def booleans(self, arr: np.ndarray) -> None:
        assert arr.ndim == 1
        super(SpectrumLabels, type(self)).booleans.fset(self, arr)

    @_Labels.scalars.setter
    def scalars(self, arr: np.ndarray) -> None:
        assert arr.ndim == 1
        super(SpectrumLabels, type(self)).scalars.fset(self, arr)

    @property
    def booleans_series(self) -> pd.Series:
        if self.booleans is None:
            return None
        return pd.Series(
            self.booleans,
            index=pd.Index(self.names, name="label"),
        )

    @property
    def dataframe(self) -> pd.DataFrame:
        if self.is_empty():
            return None
        df = pd.DataFrame(index=pd.Index(self.names, name="label"))
        if self.booleans is not None:
            df["booleans"] = self.booleans
        if self.scalars is not None:
            df["scalars"] = self.scalars
        return df

    @property
    def single_encounter_label_num(self) -> int:
        """Get the label number (index in the labels list) for this spectrum.

        Returns
        -------
        int or None
            Integer label or None if no boolean labels are available.

        Raises
        ------
        RadaiDataSetError
            If there are multi-label encounters.
        """
        if self.booleans is None:
            return None
        elif self.booleans.sum() > 1:
            raise RadaiDataSetError(
                f"Cannot extract a single_encounter_label_num for "
                f"{self.__class__.__name__} "
                f"with more than one boolean:\n{self.booleans_series}"
            )
        else:
            nz = np.nonzero(self.booleans)[0]
            return -1 if len(nz) == 0 else nz[0]


class WaterfallLabels(_Labels):
    """Labels for single waterfall [T, E] including background.

    The dimension references are:
        T = times
        E = energy
        L = labels

    Attributes
    ----------
    names : np.ndarray
        1D array of names [L, ]
    booleans : np.ndarray
        1D array of binary labels [L, ]
    scalars : np.ndarray
        2D array of scalar values [T, L]
    """

    @_Labels.booleans.setter
    def booleans(self, arr: np.ndarray) -> None:
        assert arr.ndim == 1
        super(WaterfallLabels, type(self)).booleans.fset(self, arr)

    @_Labels.scalars.setter
    def scalars(self, arr: np.ndarray) -> None:
        assert arr.ndim == 2
        super(WaterfallLabels, type(self)).scalars.fset(self, arr)

    @property
    def booleans_series(self) -> pd.Series:
        if self.booleans is None:
            return None
        return pd.Series(
            self.booleans,
            index=pd.Index(self.names, name="label"),
        )

    @property
    def scalars_dataframe(self) -> pd.DataFrame:
        if self.scalars is None:
            return None
        return pd.DataFrame(
            self.scalars,
            index=pd.Index(range(self.scalars.shape[0]), name="time_idx"),
            columns=pd.Index(self.names, name="label"),
        )

    @property
    def single_encounter_label_num(self) -> int:
        """Get the label number (index in the labels list) for this spectrum.

        Returns
        -------
        int or None
            Integer label or None if no boolean labels are available.

        Raises
        ------
        RadaiDataSetError
            If there are multi-label encounters.
        """
        if self.booleans is None:
            return None
        elif self.booleans.sum() > 1:
            raise RadaiDataSetError(
                f"Cannot extract a single_encounter_label_num for "
                f"{self.__class__.__name__} "
                f"with more than one boolean:\n{self.booleans_series}"
            )
        else:
            nz = np.nonzero(self.booleans)[0]
            return -1 if len(nz) == 0 else nz[0]


class BatchLabels(_Labels):
    """Labels for a batch of spectra [N, 1, E] or waterfalls [N, T, E] including
    background.

    The dimension references are:
        N = items
        T = times
        E = energy
        L = labels

    Attributes
    ----------
    names : np.ndarray
        1D array of source names [L, ]
    booleans : np.ndarray
        2D array of source binary labels [N, L]
    scalars : np.ndarray
        3D array of source scalar values [N, T, L]
    """

    @_Labels.booleans.setter
    def booleans(self, arr: np.ndarray) -> None:
        assert arr.ndim == 2
        super(BatchLabels, type(self)).booleans.fset(self, arr)

    @_Labels.scalars.setter
    def scalars(self, arr: np.ndarray) -> None:
        assert arr.ndim == 3
        super(BatchLabels, type(self)).scalars.fset(self, arr)

    @classmethod
    def from_spectrum_labels(cls, *spectrum_labels: SpectrumLabels):
        return cls(
            names=spectrum_labels[0].names,
            booleans=np.stack([sl.booleans for sl in spectrum_labels], axis=0),
            # The scalars are a 3D array with the second axis of length 1
            scalars=np.stack(
                [sl.scalars[np.newaxis, :] for sl in spectrum_labels], axis=0
            ),
        )

    @property
    def booleans_dataframe(self) -> pd.DataFrame:
        if self.booleans is None:
            return None
        return pd.DataFrame(
            self.booleans,
            index=pd.Index(
                range(self.booleans.shape[0]),
                name=("spectrum_idx" if self.is_spectra_batch() else "waterfall_idx"),
            ),
            columns=pd.Index(self.names, name="label"),
        )

    @property
    def scalars_dataframe(self) -> pd.DataFrame:
        if self.scalars is None:
            return None
        elif self.is_spectra_batch():
            return pd.DataFrame(
                np.squeeze(self.scalars),
                index=pd.Index(range(self.scalars.shape[0]), name="spectrum_idx"),
                columns=pd.Index(self.names, name="label"),
            )
        else:
            return pd.DataFrame(
                # Swap N/T. Flatten N/L. The first dim will be T.
                self.scalars.swapaxes(0, 1).reshape(self.scalars.shape[1], -1),
                index=pd.Index(range(self.scalars.shape[1]), name="time_idx"),
                columns=pd.MultiIndex.from_product(
                    [range(self.scalars.shape[0]), self.names],
                    names=["waterfall_idx", "label"],
                ),
            )

    def is_spectra_batch(self) -> bool:
        """Are the items a collection of spectra?

        Returns
        -------
        bool

        Raises
        ------
        RadaiDataSetError
            If no scalars array is present to check the second axis (time) length
            (1 for spectra and >1 for waterfalls.)
        """
        if self.scalars is None:
            raise RadaiDataSetError(
                "Cannot determine if BatchLabels is for spectra without a "
                "scalars array."
            )
        return self.scalars.shape[1] == 1

    def is_waterfall_batch(self) -> bool:
        """Are the items a collection of waterfalls?

        Returns
        -------
        bool

        Raises
        ------
        RadaiDataSetError
            If no scalars array is present to check the second axis (time) length
            (1 for spectra and >1 for waterfalls.)
        """
        return not self.is_spectra_batch()

    @property
    def single_encounter_label_nums(self) -> Union[np.ndarray, None]:
        """Get the label number (index in the labels list) for each item.

        Returns
        -------
        np.ndarray or None
            Array of integer labels or None if no boolean labels are available.

        Raises
        ------
        RadaiDataSetError
            If there are multi-label encounters.

        Notes
        -----
        If any items have no label, the label num will be -1.
        """
        if self.booleans is None:
            return None
        multi_encounters = self.booleans.sum(axis=1) > 1
        if np.any(multi_encounters):
            raise RadaiDataSetError(
                f"Cannot extract single_encounter_label_nums for "
                f"{self.__class__.__name__} "
                f"with {multi_encounters.sum()} multi-label encounters."
            )
        else:
            # -1 Implies the item has no label!
            indices = np.ones(self.booleans.shape[0], dtype=int) * -1
            # Get the column indices for each label
            nz = np.nonzero(self.booleans)
            indices[nz[0]] = nz[1]
            # TODO add warning for the case with missing labels!
            return indices
