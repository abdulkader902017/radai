"""Base class for DataSet specific labelers."""
import abc
import pandas as pd
from .labels import SpectrumLabels, WaterfallLabels, BatchLabels


class Labeler(abc.ABC):
    """Base class to make labels and be registered with a DataSet."""

    def __init__(self, data_set):
        self._data_set = data_set

    @property
    def data_set(self):
        return self._data_set

    @abc.abstractmethod
    def clear_caches(self) -> None:
        """Clear method lru_cache(s)"""

    @abc.abstractmethod
    def make_boolean_dataframe(
        self, for_waterfalls: bool = False, **kwargs
    ) -> pd.DataFrame:
        """Make boolean dataframe of all or a subset of the data"""

    @abc.abstractmethod
    def make_spectrum_labels(self, **kwargs) -> SpectrumLabels:
        """Make spectrum labels"""

    @abc.abstractmethod
    def make_waterfall_labels(self, **kwargs) -> WaterfallLabels:
        """Make waterfall labels"""

    @abc.abstractmethod
    def make_batch_labels(self, **kwargs) -> BatchLabels:
        """Make batch labels"""
