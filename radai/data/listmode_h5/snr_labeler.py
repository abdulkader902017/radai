from typing import Union
from functools import lru_cache
import numpy as np
import pandas as pd
from ..core import RadaiDataSetError
from ..core.labels import SpectrumLabels, WaterfallLabels, BatchLabels
from ..core.labeler import Labeler


# TODO: Add integral SNR in the dataset in the metadata
# TODO: Add peak SNR using the time bin size defined in the dataset
class ListmodeH5SNRLabeler(Labeler):
    def __init__(
        self,
        data_set,
        method="s/sqrt(s+b)",
        energy_min=None,
        energy_max=None,
        aggregation_method: str = "max",
        threshold: float = 0.1,
        threshold_bkg_only: float = 1e-6,
    ) -> None:
        """Signal to noise ratio labeler for Listmode H5 datasets

        Parameters
        ----------
        data_set : ListmodeH5
            Data set to label
        method : {"s/sqrt(b)", "s/sqrt(s+b)", "s/b", "s/(s+b)", "currie"}
            Method to calculate SNR:
                * s/sqrt(b): from Aucott 2014
                * s/sqrt(s+b): from Nicholson 2020 (default)
                * s/b: fraction of source relative to background
                * s/(s+b): fraction of source of total spectrum
                * currie: Not Implemented
        energy_min : float, optional
            Minimum energy considered, by default None (no bound).
        energy_max : float, optional
            Maximum energy considered, by default None (no bound).
        aggregation_method : str, optional
            Method for aggregating SNR per source across all the time steps in
            each waterfall. This has no effect if aggregate_for_waterfalls is
            False, by default "max"
        threshold : float, optional
            SNR threshold above which a source is considered present.
        threshold_bkg_only : float, optional
            SNR threshold below which a source is not considered present.
            Data can only be labeled BKG if there are no sources above this
            threshold.
        """
        super().__init__(data_set=data_set)
        self._method = str(method)
        self._energy_min = None if energy_min is None else float(energy_min)
        self._energy_max = None if energy_max is None else float(energy_max)
        self._aggregation_method = str(aggregation_method)
        self._threshold = float(threshold)
        self._threshold_bkg_only = float(threshold_bkg_only)
        if self._threshold_bkg_only > self._threshold:
            raise RadaiDataSetError(
                f"BKG-only threshold ({self._threshold_bkg_only}) "
                f"must be less than the labeling threshold ({self._threshold})"
            )

    def __repr__(self) -> str:
        s = "ListmodeH5SNRLabeler("
        s += ", ".join(
            [
                f"{k}={getattr(self, k)}"
                for k in (
                    "data_set",
                    "method",
                    "energy_min",
                    "energy_max",
                    "aggregation_method",
                    "threshold",
                    "threshold_bkg_only",
                )
            ]
        )
        s += ")"
        return s

    def __str__(self) -> str:
        return self.__repr__()

    def clear_caches(self) -> None:
        self.make_boolean_dataframe.cache_clear()

    @property
    def method(self):
        return self._method

    @property
    def energy_min(self):
        return self._energy_min

    @property
    def energy_max(self):
        return self._energy_max

    @property
    def aggregation_method(self):
        return self._aggregation_method

    @property
    def threshold(self):
        return self._threshold

    @property
    def threshold_bkg_only(self):
        return self._threshold_bkg_only

    @property
    def snr_kwargs(self) -> dict:
        """Keyword arguments for data_set.get_snr_for_runs

        Returns
        -------
        dict
        """
        return dict(
            method=self.method,
            energy_min=self.energy_min,
            energy_max=self.energy_max,
            aggregation_method=self.aggregation_method,
            include_all_labels=True,
            index_by_time=False,
        )

    def _snr2bool(
        self, snr: Union[pd.Series, pd.DataFrame]
    ) -> Union[pd.Series, pd.DataFrame]:
        # Sources present
        bools = snr >= self.threshold
        # Source presence is indeterminate
        indet = (self.threshold_bkg_only <= snr) & (snr <= self.threshold)
        # Label background where nothing else is present
        if isinstance(snr, pd.Series):
            bools.iloc[0] = (bools.sum() == 0) & (indet.sum() == 0)
        else:
            bools.iloc[
                (bools.sum(axis="columns") == 0) & (indet.sum(axis="columns") == 0), 0
            ] = True
        # All done!
        return bools

    @lru_cache(maxsize=10)
    def make_boolean_dataframe(
        self,
        for_waterfalls: bool = False,
        run_ids: tuple = None,
        progress_bar: bool = False,
    ) -> pd.DataFrame:
        """Create a table of boolean labels (shape of data_set.get_snr_for_runs)

        Parameters
        ----------
        for_waterfalls : bool, optional
            _description_, by default False
        run_ids : list, optional
            _description_, by default None
        progress_bar : bool, optional
            _description_, by default False

        Returns
        -------
        pd.DataFrame
            _description_
        """
        kw = self.snr_kwargs
        kw.pop("include_all_labels")
        kw.pop("index_by_time")
        kw["aggregate_for_waterfalls"] = bool(for_waterfalls)
        snr = self.data_set.get_snr_for_runs(
            run_ids=None if run_ids is None else tuple(run_ids),
            progress_bar=progress_bar,
            **kw,
        )
        return self._snr2bool(snr)

    def make_spectrum_labels(self, indices: tuple) -> SpectrumLabels:
        """Generate spectrum labels for the source ids across all runs.

        Parameters
        ----------
        indices : tuple
            Tuple of (run_id, time_idx) for the spectrum to label.

        Returns
        -------
        SpectrumLabels
        """
        run_id, time_idx = indices
        snr = self.data_set.get_snr_for_run(run_id, **self.snr_kwargs).loc[time_idx]
        return SpectrumLabels(
            names=snr.index.values.astype(np.unicode_),
            booleans=self._snr2bool(snr).values,
            scalars=snr.values,
        )

    def make_waterfall_labels(self, indices: tuple) -> WaterfallLabels:
        """Generate waterfall labels for the source ids across all runs.

        Parameters
        ----------
        indices : tuple
            Tuple of (run_id, time_idx) for the waterfall to label.

        Returns
        -------
        WaterfallLabels
        """
        run_id, time_idx = indices
        # This is a DataFrame
        # NOTE The -1 time index offset is because pandas slicing is inclusive
        snr_spec = self.data_set.get_snr_for_run(
            run_id, aggregate_for_waterfalls=False, **self.snr_kwargs
        ).loc[time_idx : time_idx + self.data_set.num_time_bins - 1]
        # This is a Series
        snr_wf = self.data_set.get_snr_for_run(
            run_id, aggregate_for_waterfalls=True, **self.snr_kwargs
        ).loc[time_idx]
        # Labels for this waterfall
        return WaterfallLabels(
            names=snr_wf.index.values.astype(np.unicode_),
            booleans=self._snr2bool(snr_wf),
            scalars=snr_spec.values,
        )

    def make_batch_labels(self, indices: list) -> BatchLabels:
        """Generate batch labels for the source ids across all runs.

        Parameters
        ----------
        indices : tuple
            List of tuples of (run_id, time_idx) for the waterfalls in the batch
            to label.

        Returns
        -------
        BatchLabels
        """
        # Get labels for each waterfall
        wfls = [self.make_waterfall_labels(idx) for idx in indices]
        # Make 2D array of source booleans [batch_size, source_id]
        booleans = np.stack([wfl.booleans for wfl in wfls], axis=0)
        # Make 3D array of source scalars [batch_size, time_idx, source_id]
        scalars = np.stack([wfl.scalars for wfl in wfls], axis=0)
        # Labels for this batch
        return BatchLabels(names=wfls[0].names, booleans=booleans, scalars=scalars)

    def copy_with_new_params(self, **params):
        """Copy with optionally updated params.

        Parameters
        ----------
        method : {"s/sqrt(b)", "s/sqrt(s+b)", "s/b", "s/(s+b)", "currie"}, optional
            Method to calculate SNR:
                * s/sqrt(b): from Aucott 2014
                * s/sqrt(s+b): from Nicholson 2020 (default)
                * s/b: fraction of source relative to background
                * s/(s+b): fraction of source of total spectrum
                * currie: Not Implemented
        energy_min : float, optional
            Minimum energy considered.
        energy_max : float, optional
            Maximum energy considered.
        aggregation_method : str, optional
            Method for aggregating SNR per source across all the time steps in
            each waterfall. This has no effect if aggregate_for_waterfalls is
            False.
        threshold : float, optional
            SNR threshold above which a source is considered present.
        threshold_bkg_only : float, optional
            SNR threshold below which a source is not considered present.
            Data can only be labeled BKG if there are no sources above this
            threshold.

        Returns
        -------
        ListmodeSNRLabeler
            New labeler with optionally updated params.
        """
        return self.__class__(
            self.data_set,
            method=params.get("method", self.method),
            energy_min=params.get("energy_min", self.energy_min),
            energy_max=params.get("energy_max", self.energy_max),
            aggregation_method=params.get(
                "aggregation_method", self.aggregation_method
            ),
            threshold=params.get("threshold", self.threshold),
            threshold_bkg_only=params.get(
                "threshold_bkg_only", self.threshold_bkg_only
            ),
        )
