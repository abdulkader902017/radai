"""Gross-counts (SPRT)"""


from .algorithm import Algorithm
from ..result import Result, Results
from ..filters import MovingAverageFilter, make_filter
from scipy.special import erfinv
import numpy as np


class SPRT(Algorithm):
    def __init__(
        self,
        bkg_filter: MovingAverageFilter,
        int_time: float = 1.0,
        alpha_0: float = 0.001,
        beta_0: float = 0.001,
        a: float = 50.0,
        auto_calculate_thresholds: bool = True,
    ):
        """Sequential Probability Ratio Test (SPRT) gross count rate algorithm

        Parameters
        ----------
        bkg_filter : dict or MovingAverageFilter
            Background filter configuration
        int_time : float, optional
            Integration time in seconds, by default 1.0
        alpha_0 : float, optional
            Target false positive probability, by default 0.01
        beta_0 : float, optional
            Target false negative probability (generally equal to alpha_0),
            by default 0.01
        a : float, optional
            Alternative hypothesis acceptance threshold (only used if
            auto_calculate_thresholds is False), by default 50.0
        auto_calculate_thresholds : bool, optional
            Calculate thresholds a and b from target error rates, by default True
        """

        self.l_n = 0.0  # Integrated log-likelihood ratios

        # Instantiate instance attributes
        super().__init__(
            bkg_filter=make_filter(bkg_filter),
            int_time=int_time,
            alpha_0=alpha_0,
            beta_0=beta_0,
            auto_calculate_thresholds=auto_calculate_thresholds,
        )
        # calculate thresholds analytically if requested
        self.a = a
        if auto_calculate_thresholds:
            self.a = self.calculate_threshold()

    def calculate_threshold(self) -> float:
        """Calculate nominal hypothesis thresholds based on target error rates

        Returns
        -------
        float
            Threshold
        """

        return np.log((1 - self.beta_0) / self.alpha_0)

    def fit(self, spectra, timestamps, live_times=None) -> None:
        """Pre-fit some background data

        Parameters
        ----------
        spectrum : float or array
            Individual spectrum, or just counts, for analysis
        timestamp : float
            Timestamp at the end of the measurement being analyzed.
        live_time : float (optional)
            Integration time of spectrum or counts, by default the integration
            time in the configuration.
        """
        if live_times is None:
            live_times = np.full_like(timestamps, fill_value=self.int_time)
        self.bkg_filter.batch_update(spectra.sum(axis=1), timestamps, live_times)

    def _analyze(self) -> Results:
        """Analyze single spectrum.

        Returns
        -------
        Results
            Analysis results. The `alarm_metric` is the sequential log
            likelihood.
        """

        spectrum, timestamp, live_time = self.spectra_buffer[-1]
        if np.isnan(live_time):
            live_time = self.int_time

        # Check if we have acquired enough background data to make an estimate
        if self.bkg_filter.is_full():
            # if we have, then calculate some statistics of the background data
            mean, var = self.bkg_filter.predict(live_time)
            std = np.sqrt(var[0, 0])

            z_i = self.calculate_log_likelihood(mean, std, spectrum.sum() / live_time)
            self.l_n += z_i
            if self.l_n < 0.0:
                self.l_n = 0.0

            # check if we should accept the alternative hypothesis (alarm)
            metric = self.l_n
            alarm = self.l_n >= self.a

            # Prepare and return results
            return Results(
                timestamp,
                [
                    Result(
                        origin="SPRT",
                        source_id=1,
                        is_alarm=alarm,
                        alarm_metric=metric,
                        alarm_metric_name="sequential-log-likelihood",
                        timestamp_start=timestamp - self.int_time,
                        timestamp_stop=timestamp,
                    )
                ],
            )
        # If the background filter is not ready, return an empty results (the
        # filter will be updated in _update_state)
        else:
            return Results(timestamp)

    def _update_state(self, results: Results) -> None:
        """Update the background filter from the SpectraBuffer ONLY IF the
        passed Results do not contain an alarm.

        Parameters
        ----------
        results : Results
        """
        # Update the background filter when the current data does not result in
        # an alarm.
        if not results.contains_alarm():
            spectrum, timestamp, live_time = self.spectra_buffer[-1]
            self.bkg_filter.update(spectrum.sum(), timestamp, live_time)

    def to_dict(self) -> dict:
        return dict(
            bkg_filter=self.bkg_filter.to_dict(),
            a=self.a,
            alpha_0=self.alpha_0,
            beta_0=self.beta_0,
            int_time=self.int_time,
            auto_calculate_thresholds=self.auto_calculate_thresholds,
        )

    def is_ready(self) -> bool:
        """Is this algorithm ready to analyze?

        Returns
        -------
        bool

        Notes
        -----
        In this case a trained algorithm needs to have its background filter
        ready to predict and 2 parameters ready to
        """
        return self._all_ready("a", "int_time") and self.bkg_filter.is_full()

    # ---------------------------- Helper Methods -----------------------------
    def calculate_nominal_net_cps(self, mu: float, sigma: float) -> float:
        """Use the inverse Gaussian CDF to calculate the approximate cps value
        below which we achieve the target FNP. We make the assumption that the
        background data are normally distributed.

        Parameters
        ----------
        mu : float
            Mean of measured background data.
        sigma : float
            Standard deviation of measured background data.

        Returns
        -------
        float
            Estimated net (background + signal) CPS value at which we achieve
            the target FNP
        """
        return mu + sigma * np.sqrt(2.0) * erfinv(2.0 * (1.0 - self.beta_0) - 1.0)

    def calculate_log_likelihood(self, mu: float, sigma: float, x_i: float) -> float:
        """Calculate the log likelihood ratio

        Parameters
        ----------
        mu : float
            Measured mean background countrate.
        sigma : float
            Measured standard deviation of background countrate.
        x_i : float
            Measured interval countrate.

        Returns
        -------
        float
            Log likelihood ratio
        """
        l1 = self.calculate_nominal_net_cps(mu, sigma)
        z_i = np.log(l1 / mu) * x_i - (l1 - mu)
        return np.nan_to_num(z_i)

    def reset(self):
        self.l_n = 0.0
        self.bkg_filter.clear()
