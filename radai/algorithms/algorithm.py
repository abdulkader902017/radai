"""Base classes for algorithm objects.
"""
import abc
from warnings import warn
from typing import Union, Any
from pathlib import Path
import numpy as np
from ..core import RadaiAlgorithmWarning
from ..io import _read, _write
from ..result import Results, ResultsBatch
from ..buffer import SpectraBuffer


class Algorithm(abc.ABC):
    """Base class for algorithms.

    The default initializer will set all kwargs as attributes unless the name
    collides with an existing object in the class namespace in which case an
    AttributeError is raised. This is included to allow developers to quickly
    initialize values and simplify loading from a dictionary of parameters loaded
    from a file (i.e. yaml or json) with the from_dict method used by the read
    method. Developers of more complex algorithms are encouraged to implement
    their own initializer and from_dict method to build their algorithm as needed.

    Developers of algorithms which cannot be loaded from dictionary-like objects
    or files will need to overwrite the read/write methods.

    Developers of algorithms which require auxillary files of data (txt, h5,
    binary, etc) will need to use the path kwarg of [to|from]_dict in order to
    load that data based on a path to the parent directory of the file(s). For
    example, an algorithm which has three parameters and a h5 data file can be
    stored like:
        pars = {'a': 1, 'b': 2.0, 'c': True, 'h5_data_file': 'my/data.h5'}
    and the loading would look like:
        MyAlgo.from_dict(pars, path='/parent/directory')
    for data here:
        /parent/directory/my/data.h5

    Properties
    ----------
    threshold : float or None
        Single threshold for the alarm_metric to determine an alarm. This is NOT
        required and is include for legacy reasons in order to support algorithms
        with a single alarm_metric and method for triggering. This attribute is
        set in the default object constructor and thus is always present.
    spectra_buffer : SpectraBuffer
        Spectra buffer to automatically buffer spectra, timestamps and
        live times upon each call to `analyze`. This buffer is always used even
        if the algorithm analyzes only one spectra or single scalar count value.
        It also supports algorithms which use multiple spectra in time sequence.
        The subclass should use `spectra_buffer_num_time` in the base constructor to
        configure the spectra length. The energy bins will be inferred
        from the first call to self._spectra_buffer.update. The developer should
        call _spectra_buffer.get_spectra/get_timestamps/get_live_times in
        ._analyze for processing

    Abstract Methods
    ----------------
    fit
        Train this algorithm using a single batch of spectra each with one
        associated label. This approach can be overriden to support any desired
        input (i.e. waterfalls, batch generators, multiple labels per item) but
        developers are encouraged to use a training module for those cases and
        having this method raise a NotImplementedError while pointing users to
        the appropriate module to train the algorithm.
    _analyze
        Analyze a single measurement using the SpectraBuffer to access the
        most recent data.
    to_dict
        Serialize the algorithm state to a dictionary while optionally writing
        support files to the `path` location with relative paths stored in the
        dictionary. In those cases, the developer will need to overwrite the
        from_dict method as well to correctly handle loading those files.
    is_ready
        Is this algorithm ready to analyze (parameters set, buffers full, etc)?

    Hidden Methods
    --------------
    _update_state
        Update the algorithm state based on a new set of results from _analyze.
    _all_ready
        Determine if all the input attributes and/or objects are ready for use.

    Methods
    -------
    analyze
        Analyze a new measurement and update the algorithm's state.
    analyze_batch
        Analyze multiple measurements in sequence.
    from_dict
        Initialize algorithm from dictionary.
    write
        Write algorithm to multiple file types.
    read
        Read algorithm from multiple filetypes.
    check_ready
        Check that the `Algorithm` is ready to analyze data and raise a
        `ValueError` if not.
    analyze_curie
        Not implemented
    calculate_threshold
        Calculate a single threshold. By default raises `NotImplementedError`.
    """

    def __new__(cls, *args, **kwargs):
        """Construct a new Algorithm and initialize attributes."""
        obj = object.__new__(cls)
        obj._threshold = None
        obj._spectra_buffer = None
        return obj

    def __init__(
        self,
        spectra_buffer_num_time: int = 1,
        spectra_buffer_num_energy: int = None,
        spectra_buffer_dtype=np.uint32,
        **kwargs,
    ):
        """Initialize an algorithm from the configuration in the kwargs.

        Parameters
        ----------
        spectra_buffer_num_time : int, optional
            Configure the number of current (and potentially past if >1) spectra
            to store. These spectra will be updated in `analyze` and available
            to the developer in the `_analyze`, `_update_state`, and other methods.
            By default, 1 (only the current spectra.)
        spectra_buffer_num_energy : int, optional
            Number of energy bins (the length of the buffered energy spectra).
            Algorithm developers can use this kwarg to set the buffer shape in
            order to prevent users from setting the buffer shape with the first
            spectrum analyzed and potentially break the `_analyze` method. By
            default None in which case the buffer will use the length of the
            first spectrum provided to the `analyze` method.
        spectra_buffer_dtype : numpy dtype, optional
            Numpy data type of the array of spectra, by default np.uint32
        kwargs : Any
            Configuration parameters for this algorithm. They will be set as
            (editable) class attributes.

        Raises
        ------
        AttributeError
            If a kwarg name collides with an existing algorithm attribute or
            method.

        Notes
        -----
        This method should NOT be overriden unless the developer also overrides
        the from_dict method which (by default) calls this init with the config
        params as the dictionary keys. The user is encouraged to use the
        from_dict or read constructor methods if asset files must be loaded to
        recreate a trained algorithm.
        """
        self._spectra_buffer = SpectraBuffer(
            n_time=spectra_buffer_num_time,
            n_energy=spectra_buffer_num_energy,
            spectra_dtype=spectra_buffer_dtype,
        )
        for k, v in kwargs.items():
            if hasattr(self, k):
                raise AttributeError(f"Cannot overwrite existing class attribute: {k}")
            setattr(self, k, v)

    # ---------------------------- Abstract Methods ----------------------------
    @abc.abstractmethod
    def fit(self, spectra: np.ndarray, spectra_ids: np.ndarray, **kwargs) -> None:
        """Train this algorithm using a single batch of spectra each with one
        associated label. This approach can be overriden to support any desired
        input (i.e. waterfalls, batch generators, multiple labels per item) but
        developers are encouraged to use a training module for those cases and
        having this method raise a NotImplementedError while pointing users to
        the appropriate module to train the algorithm.

        Parameters
        ----------
        spectra : array_like
            2D training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            1D integer label for the training set (num_measurements, ).

        Other Parameters
        ----------------
        kwargs : dict
            Algorithm specific fit parameters.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """

    @abc.abstractmethod
    def _analyze(self) -> Results:
        """Analyze a single measurement using the SpectraBuffer to access the
        most recent data.

        Returns
        -------
        Results
            Analysis results.

        Notes
        -----
        The current and optionally past (if spectra_buffer_num_time > 1) spectra,
        timestamps, and livetimes are available in the `spectra_buffer` instead
        of being passed as arguments to this method.
        """

    @abc.abstractclassmethod
    def to_dict(cls, path: Union[str, Path] = None) -> dict:
        """Serialize the algorithm state to a dictionary while optionally writing
        support files to the `path` location with relative paths stored in the
        dictionary. In those cases, the developer will need to overwrite the
        from_dict method as well to correctly handle loading those files.

        Parameters
        ----------
        path : Union[str, Path], optional
            The directory in which to write auxillary/support files for this
            algorithm's state. Include the path to these files in the dictionary
            relative to the provided `path`. Make sure to implement the loading
            of these support files in `from_dict`, by default None

        Returns
        -------
        dict
            Serialized algorithm parameters.

        Notes
        -----
        If additional files need to be written, include the relative paths in
        the dictionary and the use the parent path with this method. This method
        is intended to be used by `write` which will automatically pass the path
        to the parent directory of the file being written.
        """

    @abc.abstractmethod
    def is_ready(self) -> bool:
        """Is this algorithm ready to analyze (parameters set, buffers full, etc)?"""

    # ---------------------------- Hidden Methods ------------------------------

    def _update_state(self, results: Results) -> None:
        """Update the algorithm state based on a new set of results from _analyze.

        This method is called after _analyze in the user facing analyze method.
        Algorithm developers are encouraged to update algorithm filters and
        other state parameters in this method. See the SPRT built-in algorithm
        for an example of this pattern.

        Parameters
        ----------
        results : Results
            Results from a recent analysis to use for the state update.
        """
        pass

    def _all_ready(self, *args: Any) -> bool:
        """Determine if all the input attributes and/or objects are ready for use.

        This is a convenience method to check the attribute values. The currently
        supported cases for "not ready" are:
            * If the arg is None
            * If the arg is a string and the corresponding attribute value is None
              or has an `is_ready` method which returns `False`.
            * If the arg is an object with an `is_ready` method which returns `False`

        Parameters
        ----------
        args : Any
            Attribute names or objects to check for readiness.

        Returns
        -------
        bool
            If the algorithm attributes are ready for use.
        """

        def is_ready(arg):
            if arg is None:
                return False
            elif hasattr(arg, "is_ready"):
                return arg.is_ready()
            return True

        for arg in args:
            # Check str attributes are not None. This works for nested
            # attributes by iterating over the attributes between periods (dots).
            if isinstance(arg, str):
                this = self
                for attr in arg.split("."):
                    this = getattr(this, attr, None)
                    if not is_ready(this):
                        return False
            elif not is_ready(arg):
                return False
        return True

    # --------------------------- Public Properties ----------------------------

    @property
    def threshold(self) -> Union[float, None]:
        """Single threshold for the alarm_metric to determine an alarm. This is NOT
        required and is include for legacy reasons in order to support algorithms
        with a single alarm_metric and method for triggering. This attribute is
        set in the default object constructor and thus is always present.

        Returns
        -------
        float or None
        """
        return self._threshold

    @threshold.setter
    def threshold(self, arg: Union[float, None]) -> None:
        """Set the single alarm metric threshold.

        Parameters
        ----------
        arg : float or None
            Alarm metric threshold which will be cast to a float.
        """
        if arg is None:
            self._threshold = None
        else:
            self._threshold = float(arg)

    @property
    def spectra_buffer(self) -> SpectraBuffer:
        """Spectra buffer to automatically buffer spectra, timestamps and
        live times upon each call to `analyze`.

        This buffer is always used even if the algorithm analyzes only one
        spectra or single scalar count value. It also supports algorithms which
        use multiple spectra in time sequence. The subclass should use
        `spectra_buffer_num_time` in the base constructor to configure the
        spectra length. The energy bins will be inferred from the first call to
        self._spectra_buffer.update. The developer should call
        _spectra_buffer.get_spectra/get_timestamps/get_live_times in ._analyze
        for processing

        Returns
        -------
        SpectraBuffer
        """
        return self._spectra_buffer

    # ---------------------------- Public Methods ------------------------------

    def analyze(
        self,
        spectrum: np.ndarray,
        timestamp: float = None,
        live_time: float = None,
        **kwargs,
    ) -> Results:
        """Analyze a new measurement and update the algorithm's state.

        The analysis works is as follows:
            * update the `SpectraBuffer`
            * check if the algorithm is_ready
                * if the algorithm is ready, get the `Results` from `_analyze`
                * otherwise the `Results` will be empty
            * update the algorthm state with the `Results` in the `_update_state`
              method

        Parameters
        ----------
        spectrum : int or array_like
            Individual spectrum, or just scalar counts, for analysis.
        timestamp : float, optional
            Timestamp at the end of the measurement being analyzed. Defaults to
            np.nan
        live_time : float, optional
            Live time of the measurement. Defaults to np.nan

        Other Parameters
        ----------------
        kwargs : Any
            Additional kwargs passed to the child class _analyze method.

        Returns
        -------
        Results
            Results from this analysis. If algorithm is not ready, an empty
            Results is returned.

        Raises
        ------
        TypeError
            If the developer-implemented _analyze method does not return a Results
            object.

        Notes
        -----
        The defaults for timestamp and live_time are optional because some
        algorithms may not use this information. These will be buffered as NaN's
        in the `SpectraBuffer`. It is up to the algorithm developer to handle
        the null causes for timestamp and livetime and raise an appropriate and
        informative error in `_analyze`, `_update_state`, etc.
        """
        self.spectra_buffer.update(spectrum, timestamp, live_time)
        if not self.is_ready():
            warn(
                f"{self.__class__.__name__} is not ready to analyze. "
                "Returning empty Results",
                RadaiAlgorithmWarning,
            )
            results = Results(timestamp=timestamp)
        else:
            results = self._analyze(**kwargs)
            if not isinstance(results, Results):
                raise TypeError(
                    "Algorithm.analyze must return a Results not: "
                    f"({type(results)}){results}"
                )
        self._update_state(results)
        return results

    def analyze_batch(self, spectra, timestamps, live_times=None) -> ResultsBatch:
        """Analyze multiple measurements in sequence.

        This is a convenience method. The spectra measurements will be processed
        in order which could cause issues for algorithms with a spectra buffer
        if the spectra in this list are not all in time sequence. For more
        complicated behavior the developer is encouraged to override this method.

        Parameters
        ----------
        spectra : 2D array (n_measurements, n_bins)
            Set of spectra for analysis
        timestamps : 1D array
            Timestamps at the end of each measurement being analyzed.
        live_times : 1D array (optional)
            Integration time for each measurement

        Returns
        -------
        ResultsBatch
            Results objects containing the output of analyze for each spectrum.
        """
        live_times = [None] * len(spectra) if live_times is None else live_times
        return ResultsBatch(
            [
                self.analyze(s, t, lt)
                for s, t, lt in zip(spectra, timestamps, live_times)
            ]
        )

    @classmethod
    def from_dict(cls, dic: dict, path: Union[str, Path] = None):
        """Initialize algorithm from dictionary.

        If additional files need to be loaded, include the path to these files
        relative to the `path` kwarg.

        Parameters
        ----------
        dic : dict
            Algorithm init kwargs.

        Other Parameters
        ----------------
        path : Union[str, Path], optional
            Unused but included for compatibility with read, by default None

        Returns
        -------
        Algorithm
            The initialized algorithm.
        """
        return cls(**dic)

    def write(self, filename: Union[str, Path]) -> None:
        """Write algorithm to multiple file types.

        See `radai.io.write` for more details.

        Parameters
        ----------
        filename : Union[str, Path]
            Path to the writing location on the local system. This will overwrite
            existing files. The supported write targets are:
                * directory: the obj will be attempted to be written to data.yaml
                    inside this location.
                * tar|tgz|tar.gz|tar.bz2|tar.xz: tar archive of the directory
                    written by this function for the same arg input. The directory
                    is written to a temporary location before adding to the desired
                    archive target.
                * p|pkl: a python pickle
                * json: a json string in a text file
                * yml|yaml: a yaml string in a text file
        Notes
        -----
        Yaml, json, tar and directory serialization use from_dict to initialize
        the algorithm while pickle serialization just dumps the object as-is.
        """
        _write(self, filename)

    @classmethod
    def read(cls, filename: Union[str, Path]):
        """Read algorithm from multiple filetypes.

        See `radai.io.read` for more details.

        Parameters
        ----------
        filename : Union[str, Path]
            Path to the file or directory to read from. Currently supports:
                * directory: read from a directory containing a data.yaml file.
                * tar|tgz|tar.gz|tar.bz2|tar.xz: read a tar archive containing
                    the contents which can be read as a dictionary by this
                    function. The directory is extracted to a temporary location
                    before loading.
                * p|pkl: read a python object directly from a pickle file. Just
                    a thin wrapper around pickle.loads
                * yaml|yml|json: load the data with the respective library. If
                    it is not a dictionary, return it. Otherwise try to import a
                    class from the __class__ key value and load with the
                    `from_dict` method of the algorithm.

        Returns
        -------
        Algorithm
            Initialized algorithm from the loaded data.

        Notes
        -----
        Yaml, json, tar and directory loaders use from_dict to initialize the
        algorithm while pickle deserialization just loads the object as-is.
        """
        obj = _read(filename)
        # Backwards compatibility for dictionaries without __class__
        if isinstance(obj, dict):
            return cls.from_dict(obj, path=filename.parent)
        elif not isinstance(obj, cls):
            raise TypeError(f"Cannot read a {cls} from a {type(obj)}")
        else:
            return obj

    def check_ready(self) -> None:
        """Check that the `Algorithm` is ready to analyze data.

        Raises
        ------
        ValueError
            If `is_ready()` returns False.
        """
        if not self.is_ready():
            raise ValueError("Algorithm is not ready to analyze.")

    # ---------------------------- Not implemented Methods ---------------------

    def analyze_curie(self, curie_data) -> Union[Results, ResultsBatch]:
        raise NotImplementedError("An Algorithm cannot yet use curie data.")

    def calculate_threshold(self, set_threshold: bool = True) -> float:
        """Calculate a single threshold. By default raises `NotImplementedError`.

        Parameters
        ----------
        set_threshold : bool, optional
            Whether to update the threshold for this algorithm., by default True

        Returns
        -------
        float
            Threshold value

        Raises
        ------
        NotImplementedError
            By default. The intention is for a developer to implement this.
        """
        raise NotImplementedError(
            "This algorithm doesn't have a threshold calculation."
        )
