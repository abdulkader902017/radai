"""MLP algorithm."""

try:
    from tensorflow import keras
except ImportError:
    keras = None

from radai.result import Result
from .nn_classifier import NN_Classifier

MLP_CONFIG = {
    "time_dim": 1,
    "energy_dim": None,
    "mlp": [25, 25],
    "n_sources": None,
    "activation": "gelu",
    "threshold": 0.8,
    "dropout": 0.25,
    "int_time": 1.0,
}


def check_nn_config(nn_config):
    for item in MLP_CONFIG.keys():
        assert item in nn_config
        assert nn_config[item] is not None


def build_model(nn_config):
    # build the main model
    inputs = keras.Input(
        shape=(nn_config["time_dim"], nn_config["energy_dim"]), name="input"
    )

    # roll MLP layers
    for idx, num_neurons in enumerate(nn_config["mlp"]):
        if idx == 0:
            x = inputs
        x = keras.layers.Dense(
            num_neurons, activation=nn_config["activation"], use_bias=False
        )(x)
        x = keras.layers.BatchNormalization()(x)
        x = keras.layers.Dropout(nn_config["dropout"])(x)

    # final layer to source classification plus background then softmax
    x = keras.layers.Dense(
        nn_config["n_sources"] + 1, activation=nn_config["activation"], use_bias=False
    )(x)
    # pool to squash a dimension
    x = keras.layers.GlobalAveragePooling1D(data_format="channels_last")(x)
    outputs = keras.layers.Softmax()(x)

    model = keras.Model(inputs, outputs, name="MLPNetwork")
    print(model.summary())

    # Load weights if available
    if ("checkpoint_path" in nn_config) and (nn_config["checkpoint_path"] is not None):
        model.load_weights(nn_config["checkpoint_path"])
        is_trained = True
    else:
        is_trained = False

    return model, is_trained, nn_config["threshold"]


class MLP(NN_Classifier):
    """Multi-layer perceptron spectrum classifier"""

    def __init__(
        self,
        name: str = "MLP",
        **nn_config,
    ):
        """ """
        super().__init__(name=name, **nn_config)

    def _check_nn_configs(self, nn_config):
        check_nn_config(nn_config=nn_config)

    def _build_model(self, nn_config):
        """ """
        self._check_nn_configs(nn_config)
        self.model, self._is_trained, self.threshold = build_model(nn_config)

    def _output_to_result(self, idx, output, timestamp):
        alarm = False
        if idx > 0 and output > self.threshold:
            alarm = True

        return Result(
            origin=__class__.__name__,
            source_id=idx,
            is_alarm=alarm,
            alarm_metric=output,
            alarm_metric_name="softmax",
            timestamp_start=timestamp
            - self.nn_config["int_time"] * self.nn_config["time_dim"],
            timestamp_stop=timestamp,
        )
