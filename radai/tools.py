from collections import UserList
from collections.abc import Mapping, Sequence, MutableSet
from typing import Any, Union
from abc import ABC, abstractproperty
from copy import deepcopy
import numpy as np


def to_simple_type(arg: Any) -> Union[dict, list, str, int, float, bool]:
    """Sanitize value to a list, str, int, float or bool

    Parameters
    ----------
    arg : Any
        Value to sanitize with the following mappings:
                                     None -> None
                  (str, int, float, bool) -> unchanged
                              numpy array -> list (recursive)
                                    bytes -> str
                                  mapping -> dict (recursive)
                                 sequence -> list (recursive)
                              mutable set -> list (recursive)
                                    tuple -> list (recursive)
                               np scalars -> native scalars
                          everything else -> raise ValueError

    Returns
    -------
    Union[dict, list, str, int, float, bool]
        Sanitized value ready for storage.

    Raises
    ------
    ValueError
        If the input cannot be sanitized
    """
    if arg is None:
        return None
    elif isinstance(arg, (np.floating, np.integer, np.bool_)):
        return arg.item()
    elif isinstance(arg, (str, int, float, bool)):
        return arg
    elif isinstance(arg, np.ndarray):
        return arg.tolist()
    elif isinstance(arg, bytes):
        return str(arg)
    elif isinstance(arg, Mapping):
        return {str(k): to_simple_type(v) for k, v in arg.items()}
    elif isinstance(arg, (Sequence, MutableSet, set, tuple)):
        return [to_simple_type(v) for v in arg]
    else:
        raise ValueError(f"Cannot sanitize to python native type: {arg}")


def is_close(x0, x1, **kwargs):
    # Incompatible types
    if (isinstance(x0, Mapping) and not isinstance(x1, Mapping)) or (
        isinstance(x0, (str, bytes)) and not isinstance(x1, (str, bytes))
    ):
        return False
    # Mappings
    if isinstance(x0, Mapping) and isinstance(x1, Mapping):
        if set(x0.keys()) != set(x1.keys()):
            return False
        for k in x0:
            if not is_close(x0[k], x1[k]):
                return False
        return True
    # Strings (cases issues with np.isclose)
    if isinstance(x0, (str, bytes)) and isinstance(x1, (str, bytes)):
        return x0 == x1
    # Numbers, booleans, sequences, arrays
    try:
        return np.all(np.isclose(x0, x1, **kwargs))
    except Exception:
        try:
            if len(x0) != len(x1):
                return False
            for v0, v1 in zip(x0, x1):
                if not is_close(v0, v1):
                    return False
            return True
        except Exception:
            return x0 == x1


def dict_like_is_equal(d0, d1):
    if set(d0.keys()) != set(d1.keys()):
        return False
    for k in d0:
        v0 = to_simple_type(d0[k])
        v1 = to_simple_type(d1[k])
        if isinstance(v0, dict):
            if not dict_like_is_equal(v0, v1):
                return False
        else:
            if v0 != v1:
                return False
    return True


def fpr_from_far(far, int_time):
    """
    far : False alarm rate (alarms per hour)
    int_time : Time per evaluation (seconds)

    fpr : False positive rate (per evaluation)
    """
    return far / 3600.0 * int_time


def calculate_num_fa(n_samples, far, int_time, num_hypotheses=1):
    """Calculate the number of false alarms expected based on FAR,
    int_time, and number of samples.
    """
    # Perform a Bonferroni correction when multiple sources are tested
    fpr = fpr_from_far(far, int_time)
    if num_hypotheses > 1:
        num_fa = int(np.ceil(fpr * n_samples / num_hypotheses))
    else:
        num_fa = int(np.ceil(fpr * n_samples))

    if num_fa == 0:
        print("Not enough samples, num_fa = 0")
    return num_fa


def compute_threshold_empirical(metric, num_fa):
    if num_fa == 0:
        print("num_fa = 0, computing incorrect empirical threshold.")
    return sorted(np.atleast_1d(metric), reverse=True)[num_fa - 1]


def hist_outline(hist, zero=0.0, endpoints=True):
    """Reform histogram x bins and y values to make a histogram outline.

    Parameters
    ----------
    hist : tuple of array_like
        Numpy histogram of (counts, bin_edges).
    zero : float (optional)
        Value to insert at the endpoints.
    endpoints : bool (optional)
        Whether to insert `zero` values at first edge and last edge.

    Returns
    -------
    x : array_like
        X values of the outline
    y : array_like
        Y values of the outline
    """
    i = np.arange(len(hist[1]))
    x = np.zeros(2 * len(i))
    x[2 * i] = hist[1]
    x[2 * i + 1] = hist[1]
    y = np.zeros(2 * len(i))
    y[1 + 2 * i[:-1]] = hist[0][i[:-1]]
    y[2 + 2 * i[:-1]] = hist[0][i[:-1]]
    y[0] = zero
    y[-1] = zero
    if not endpoints:
        x = x[1:-1]
        y = y[1:-1]
    y[y < zero] = zero
    return x, y


def sqrt_bins(
    bin_edge_min: float = 0.0, bin_edge_max: float = 3000.0, num_bins: int = 128
) -> np.ndarray:
    """Bin edges with bin widths increasing proportional to center**2.

    Parameters
    ----------
    bin_edge_min : float, optional
        Minimum bin edge, must be >=0, by default 0.0
    bin_edge_max : float, optional
        Maximum bin edge (must be greater than bin_min), by default 3000.0
    num_bins : int, optional
        Number of bins, by default 128

    Returns
    -------
    np.ndarray
        Array of bin edges of length num_bins + 1.

    Notes
    -----
    This was cribbed from becquerel:
        https://github.com/lbl-anp/becquerel/blob/09f710ea7a1924318f914c17c87a7ad55eaac005/becquerel/core/utils.py
    """
    assert bin_edge_min >= 0
    assert bin_edge_max > bin_edge_min
    return np.linspace(np.sqrt(bin_edge_min), np.sqrt(bin_edge_max), num_bins + 1) ** 2


class SingleTypeList(ABC, UserList):
    def __init__(self, data=None):
        self.data = []
        if data is not None:
            for item in data:
                self.append(item)

    @abstractproperty
    def ITEM_TYPE(self):
        """Type of item in this list container."""

    @abstractproperty
    def UNIQUE_ITEM_ATTRIBUTES(self):
        """Set of attribute names which must have unique values among items."""

    def __setitem__(self, i: int, item) -> None:
        if isinstance(i, slice):
            return NotImplemented
        del self[i]
        self.insert(i, item)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        return super().__eq__(other)

    def __copy__(self):
        return self.copy()

    def __deepcopy__(self, memo):
        return self.copy()

    def append(self, item) -> None:
        """Append an item at the end.

        Parameters
        ----------
        item : ITEM_TYPE
            Item to append.
        """
        self.insert(len(self), item)

    def insert(
        self,
        i: int,
        item,
    ) -> None:
        """Insert an item at position `i`.

        Parameters
        ----------
        i : int
            Index position to insert the Result.
        item : ITEM_TYPE
            Item to insert.

        Raises
        ------
        TypeError
            If the item is not a ITEM_TYPE.
        ValueError
            If there are duplicates of attributes in UNIQUE_ITEM_ATTRIBUTES.
        """
        if not isinstance(item, self.ITEM_TYPE):
            raise TypeError(f"Cannot insert {type(item)} to {self.__class__.__name__}")
        for name in self.UNIQUE_ITEM_ATTRIBUTES:
            if getattr(item, name) in self.get_all(name):
                raise ValueError(
                    f"Cannot insert {type(item)} with existing attribute: "
                    f"{name}={getattr(item, name)}"
                )
        self.data.insert(i, item)

    def get_all(self, name: str) -> list:
        """Create a list of the attribute `name` for all Result objects.

        Parameters
        ----------
        name : str

        Returns
        -------
        list
            Collection of attribute values for each Result.
        """
        return [getattr(item, str(name)) for item in self]

    def copy(self):
        """Make a deep copy of every item and return a new object.

        Returns
        -------
        class(self)
            New object of this class with deepcopied items.
        """
        return self.__class__([deepcopy(r) for r in self])
