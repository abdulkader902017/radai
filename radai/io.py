from typing import Union, Any
from pathlib import Path
import pickle
import yaml
import json
import importlib
import tarfile
from tempfile import mkdtemp
from .tools import to_simple_type


DEFAULT_BASENAME = "data.yaml"


class RadaiIOError(IOError):
    """radai io read/write errors."""


def _write(arg: Any, target: Union[str, Path]) -> None:
    """Low level object writing to multiple file formats.

    Parameters
    ----------
    arg : Any
        Any object which can be written to a file. For dictionary-like encodings
        the `to_dict` method of the object will be used if present. Otherwise
        this will attempt to simplify the object and dump it to a yaml or json
        accordingly.
    target : Union[str, Path]
        Path to the writing location on the local system. The write targets
        supported are:
            * directory: the obj will be attempted to be written as a yaml
            * tar|tgz|tar.gz|tar.bz2|tar.xz: tar archive of the directory
                written by this function for the same arg input. The directory
                is written to a temporary location before adding to the desired
                archive target.
            * p|pkl: a python pickle
            * json: a json string in a text file
            * yml|yaml: a yaml string in a text file

    Raises
    ------
    RadaiIOError
        If the filetype is not supported.
    """
    target = target if isinstance(target, Path) else Path(target)
    # Write to a directory
    if not target.suffix:
        target.mkdir(parents=True, exist_ok=True)
        _write(arg, target / DEFAULT_BASENAME)
    # Read from a compatible tar file
    elif "".join(target.suffixes) in (".tar", ".tgz", ".tar.gz", ".tar.bz2", ".tar.xz"):
        mode = "w"
        if target.suffix.endswith("gz"):
            mode += ":gz"
        elif target.suffix.endswith("bz2"):
            mode += ":bz2"
        elif target.suffix.endswith("xz"):
            mode += ":xz"
        with tarfile.open(target, mode=mode) as tar:
            tempdir = Path(mkdtemp(prefix="radai_"))
            print(f"Creating {tempdir} for tar'ng here: {target}")
            _write(arg, tempdir)
            for x in tempdir.iterdir():
                tar.add(x, arcname=x.relative_to(tempdir))
    # Binary serialization
    elif target.suffix.lower() in (".p", ".pkl"):
        target.write_bytes(pickle.dumps(arg))
    # Text file
    elif target.suffix.lower() in (".yaml", ".yml", ".json"):
        try:
            obj = arg.to_dict(path=target.parent)
            obj["__class__"] = f"{arg.__class__.__module__}.{arg.__class__.__name__}"
        except TypeError:
            # Handle the case where to_dict doesn't want a path
            obj = arg.to_dict()
            obj["__class__"] = f"{arg.__class__.__module__}.{arg.__class__.__name__}"
        except AttributeError:
            # Handle the case when the to_dict method is not present
            obj = arg
        # Save json
        if target.suffix.lower() == ".json":
            target.write_text(json.dumps(to_simple_type(obj)))
        # Save yaml
        else:
            target.write_text(yaml.dump(to_simple_type(obj)))
    # Not implemented
    else:
        raise RadaiIOError(f"Cannot write {type(arg)} to {type(target)}: {target}")


def write(arg: Any, target: Union[str, Path]) -> None:
    """Write any object to a file.

    Parameters
    ----------
    arg : Any
        Any object which can be written to a file. If the obj has a `write` method
        it will be used. Otherwise for dictionary-like encodings the `to_dict`
        method of the object will be used and if that fails this will attempt to
        simplify the object and dump it to a yaml or json accordingly.
    target : Union[str, Path]
        Path to the writing location on the local system. This will overwrite
        existing files. The supported write targets are:
            * directory: the obj will be attempted to be written to data.yaml
                inside this location.
            * tar|tgz|tar.gz|tar.bz2|tar.xz: tar archive of the directory
                written by this function for the same arg input. The directory
                is written to a temporary location before adding to the desired
                archive target.
            * p|pkl: a python pickle
            * json: a json string in a text file
            * yml|yaml: a yaml string in a text file

    Raises
    ------
    RadaiIOError
        If the filetype is not supported.
    """
    target = target if isinstance(target, Path) else Path(target)
    try:
        arg.write(target)
    except AttributeError:
        _write(arg, target)


def _read(target: Union[str, Path]) -> Any:
    """Low level object reading from a file or directory.

    Parameters
    ----------
    target : Union[str, Path]
        Path to the file or directory to read from. Currently supports:
            * directory: read from a directory containing a data.yaml file.
            * tar|tgz|tar.gz|tar.bz2|tar.xz: read a tar archive containing the
                contents which can be read as a dictionary by this function. The
                directory is extracted to a temporary location before loading.
            * p|pkl: read a python object directly from a pickle file. Just a
                thin wrapper around pickle.loads
            * yaml|yml|json: load the data with the respective library. If it is
                not a dictionary, return it. Otherwise try to import a class from
                the __class__ key value and load with the from_dict method of the
                class.

    Returns
    -------
    Any
        The loaded object.

    Raises
    ------
    RadaiIOError
        If the filetype is not supported.
        If a directory or tar archive input does not contain a data.yaml to load.
    """
    target = target if isinstance(target, Path) else Path(target)
    if not target.exists():
        raise RadaiIOError(f"Cannot find file or directory to load: {target}")
    # Read from a directory
    if target.is_dir():
        contents = [x.name for x in target.resolve().iterdir()]
        if DEFAULT_BASENAME not in contents:
            raise RadaiIOError(
                f"Cannot load directory ({target}) without a {DEFAULT_BASENAME}. "
                f"The directory has the following contents: {contents}"
            )
        return _read(target / DEFAULT_BASENAME)
    # Read from a compatible tar file
    elif tarfile.is_tarfile(target):
        with tarfile.open(target, mode="r") as tar:
            members = tar.getnames()
            if DEFAULT_BASENAME not in members:
                raise RadaiIOError(
                    f"Cannot load tar archive ({target}) without a {DEFAULT_BASENAME}. "
                    f"Tar archive has the following members: {members}"
                )
            tempdir = Path(mkdtemp(prefix="radai_"))
            print(f"Unpacking {target} for loading here: {tempdir}")
            tar.extractall(tempdir)
            return _read(tempdir)
    # Pickle serialization
    elif target.suffix.lower() in (".p", ".pkl"):
        return pickle.loads(target.read_bytes())
    # Text file
    elif target.suffix.lower() in (".yaml", ".yml", ".json"):
        # Read data from file
        if target.suffix.lower() == ".json":
            data = json.loads(target.read_text())
        else:
            data = yaml.load(target.read_text(), Loader=yaml.FullLoader)
        # If not a dictionary return it
        if not isinstance(data, dict):
            return data
        # Try to get a class definition
        cls_name = str(data.pop("__class__", ""))
        # If no class definition (module.class, etc) return
        if len(cls_name.split(".")) < 2:
            return data
        # Import the class (raises if class cannot be imported)
        mod = importlib.import_module(".".join(cls_name.split(".")[:-1]))
        cls = getattr(mod, cls_name.split(".")[-1])
        # Build the object
        return cls.from_dict(data, path=target.parent)
    # Not implemented
    else:
        raise RadaiIOError(f"Cannot read from {type(target)}: {target}")


def read(target: Union[str, Path]) -> Any:
    """Low level object reading from a file or directory.

    Parameters
    ----------
    target : Union[str, Path]
        Path to the file or directory to read from. Currently supports:
            * directory: read from a directory containing a data.yaml file.
            * tar|tgz|tar.gz|tar.bz2|tar.xz: read a tar archive containing the
                contents which can be read as a dictionary by this function. The
                directory is extracted to a temporary location before loading.
            * p|pkl: read a python object directly from a pickle file. Just a
                thin wrapper around pickle.loads
            * yaml|yml|json: load the data with the respective library. If it is
                not a dictionary, return it. Otherwise try to import a class from
                the __class__ key value and load with the from_dict method of the
                class.

    Returns
    -------
    Any
        The loaded object.

    Raises
    ------
    NotImplementedError
        If the filetype is not supported.

    Notes
    -----
    Currently this is just an interface to wrap the internal _read utility. It
    is separated to allow a future implementation to load sub-objects
    recursively.
    """
    return _read(target)
