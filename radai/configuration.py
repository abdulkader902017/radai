"""Parent class for algorithm parameters and configurations.
"""
from collections.abc import MutableMapping
from typing import Union, Any
from pathlib import Path
from .io import _read, _write
from .tools import to_simple_type, dict_like_is_equal


class Configuration(MutableMapping):
    """Dict like object for storing necessary parameters/data for algorithms"""

    def __init__(self, **kwargs):
        """Create Configuration

        Other Parameters
        ----------------
        kwargs : dict, optional
            Key, value pairs to store. The keys will be converted to strings and
            the values will be converted to python natives using
            radai.configuration.sanitize_value.

        """
        self._data = dict()
        self.update(dict(**kwargs))

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key: Union[int, float, str], value: Any):
        """Add key, value mapping.

        Parameters
        ----------
        key : Union[int, float, str]
            Key name which will be coerced to a string.
        value : Any
            Value which will be converted python natives using
            radai.configuration.sanitize_value.
        """
        key, value = str(key), to_simple_type(value)
        self._data[key] = value
        setattr(self, key, value)

    def __delitem__(self, key):
        del self._data[key]

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        return dict_like_is_equal(self, other)

    def keys(self):
        keys = super().keys()
        return tuple(keys)

    def __str__(self):
        out = f"{self.__class__.__name__}("
        if len(self) > 0:
            out += "\n"
            for k, v in self.items():
                out += f"    {k}={v}\n"
        return f"{out})"

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"{', '.join([f'{k}={v}' for k, v in self.items()])}"
            f")"
        )

    def write(self, target: Union[str, Path]):
        """Write to file.

        Parameters
        ----------
        target : Union[str, Path]
            File path to write to (will overwrite exisiting). Supports yaml,
            yml, json, p, pkl.

        Raises
        ------
        NotImplementedError
            If file has an unsupported extension.
        """
        _write(self._data, target)

    @classmethod
    def read(cls, target: Union[str, Path]):
        """Read from file.

        Parameters
        ----------
        target : Union[str, Path]
            File path to read from. Supports yaml, yml, json, p, pkl.

        Returns
        -------
        Configuration
            Initialized configuration from loaded data.

        Raises
        ------
        NotImplementedError
            If file has an unsupported extension.
        """
        return cls(**_read(target))
