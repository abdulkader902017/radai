"""Exponentially Weighted Moving Average (EWMA) filters for count rates."""

import numpy as np
from .filter import CountRateFilter, _count_rate_and_cov


class EWMAFilter(CountRateFilter):
    """Exponentially weighted moving average filter for count rate data.

    The EWMA is essentially a simplified Kalman filter that you get when you
    assume the state only evolves under noise (a random walk) and the process
    and measurement noise covariances are constant.

    The EWMA update rule is
        x_k = (1 - lambda) x_{k-1} + lambda z_k

    where z_k is the k-th count rate measurement and x_k is the filter value.

    The variance of the count rate is estimated using the following rule:
        P_k = (1 - lambda) P_{k-1} + lambda (z_k - x_{k-1})**2
    """

    _attrs = ["lam", "x_k", "P_k"]

    def __init__(self, lam=0.1, x_k=None, P_k=None, **kwargs):
        """Initialize the filter and check the parameters.

        Parameters
        ----------
        lam : float
            The exponential parameter used in the update rules.
        x_k : float or array_like
            The mean to initialize with.
        P_k : float or array_like
            The covariance to initialize with.
        """
        # check parameter value
        assert lam >= 0.0
        assert lam <= 1.0
        # call base init
        kwargs["lam"] = lam
        kwargs["x_k"] = x_k
        kwargs["P_k"] = P_k
        super().__init__(**kwargs)

    def predict(self, live_time):
        """Predict the mean and covariance matrix of the next measurement.

        Parameters
        ----------
        live_time : float
            The live time of the next measurement. Used to scale the count
            rate and rate covariance to the measurement time.

        Returns
        -------
        mean : array_like
            The predicted mean of the spectrum, a 1D array.
        covariance_matrix : array_like
            The predicted covariance of the spectrum, a 2D array.
        """
        assert self.x_k is not None
        # scale by the measurement live time
        mean = self.x_k * live_time
        cov = self.P_k * live_time**2
        return mean, cov

    def update(self, spectrum, timestamp, live_time):
        """Update the filter with the current measurement.

        Parameters
        ----------
        spectrum : int or array_like
            The measured spectrum, either an integer or 1D array.
        timestamp : float
            The timestamp of the measurement.
        live_time : float
            The live time of the measurement.
        """
        # calculate count rate measurement and covariance matrix
        z_k, R_k = _count_rate_and_cov(spectrum, live_time)

        # update step
        if self.x_k is None:
            # initialize the state if this is the first update
            self.x_k = z_k
            self.P_k = R_k
        else:
            x_k1 = np.array(self.x_k)
            self.x_k = (1 - self.lam) * self.x_k + self.lam * z_k
            self.P_k = (1 - self.lam) * self.P_k + self.lam * np.outer(
                z_k - x_k1, z_k - x_k1
            )


class EWMAFilterAlternate(EWMAFilter):
    """Exponentially weighted moving average filter for count rate data.

    This version of EWMA tracks the variance in a different way from
    the default EWMA filter. The update rule is made to track the variance
    of the mean instead of the variance of the measurements:
        P_k = (1 - lambda)**2 P_{k-1} + lambda**2 z_k
    """

    def predict(self, live_time):
        """Predict the mean and covariance matrix of the next measurement.

        Parameters
        ----------
        live_time : float
            The live time of the next measurement. Used to scale the count
            rate and rate covariance to the measurement time.

        Returns
        -------
        mean : array_like
            The predicted mean of the spectrum, a 1D array.
        covariance_matrix : array_like
            The predicted covariance of the spectrum, a 2D array.
        """
        assert self.x_k is not None
        # scale by the measurement live time
        mean = self.x_k * live_time
        cov = self.P_k * live_time**2
        # add measurement covariance to the state covariance
        # this accounts for Poisson uncertainty in the measurement
        cov += np.diag(mean)
        return mean, cov

    def update(self, spectrum, timestamp, live_time):
        """Update the filter with the current measurement.

        Parameters
        ----------
        spectrum : int or array_like
            The measured spectrum, either an integer or 1D array.
        timestamp : float
            The timestamp of the measurement.
        live_time : float
            The live time of the measurement.
        """
        # calculate count rate measurement and covariance matrix
        z_k, R_k = _count_rate_and_cov(spectrum, live_time)

        # update step
        if self.x_k is None:
            # initialize the state if this is the first update
            self.x_k = z_k
            self.P_k = R_k
        else:
            self.x_k = (1 - self.lam) * self.x_k + self.lam * z_k
            self.P_k = (1 - self.lam) ** 2 * self.P_k + self.lam**2 * R_k
