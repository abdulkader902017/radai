"""Filter objects for static and time-dependent modeling of count rates."""

import abc
import importlib
import inspect
import numpy as np


def _count_rate_and_cov(counts, live_time):
    """Calculate the count rate and its covariance matrix.

    Assumes Poisson statistics for the counts so the covariance matrix
    is diagonal. Clips the Poisson variance at 1 if the counts are zero.

    Parameters
    ----------
    counts : int or array_like
        The measured counts, either an integer or 1D array.
    live_time : float
        The live time for the measurement in seconds.

    Returns
    -------
    count_rate : array_like
        The count rate, a 1D array.
    covariance_matrix : array_like
        The count rate covariance matrix, a 2D array.
    """
    counts = np.atleast_1d(counts).astype(float)
    assert len(counts.shape) == 1
    return counts / live_time, np.diag(counts.clip(1)) / live_time**2


def _count_rate_sample_mean_median_cov(counts, live_times):
    """Calculate sample mean, median, and covariance matrix of the count rates.

    If there is only one observation, falls back to using a covariance
    matrix based on the Poisson statistics of the single measurement.

    Parameters
    ----------
    counts : array_like
        If 1D assume single feature and shape is (n_observations,).
        if 2D assume shape is (n_observations, n_bins).
    live_times : array_like
        The live times, a 1D array of length n_observations.

    Returns
    -------
    mean : array_like
        Mean count rate by bin, a 1D array.
    median : array_like
        Median count rate by bin, a 1D array.
    covariance_matrix : array_like
        Sample covariance matrix of count rates, a 2D array.
    """
    # check the inputs
    counts = np.atleast_1d(counts).astype(float)
    live_times = np.atleast_1d(live_times).astype(float)
    assert len(counts.shape) == 1 or len(counts.shape) == 2
    assert len(live_times.shape) == 1
    assert counts.shape[0] == live_times.shape[0]

    # calculate the mean rate
    if len(counts.shape) == 1:
        counts.shape = (counts.shape[0], 1)
    n, m = counts.shape
    rate_mean = counts.sum(axis=0) / live_times.sum(axis=0)

    # calculate the median rate
    rates = counts / live_times[:, None]
    rate_median = np.median(rates, axis=0)

    # if single observation, use Poisson; otherwise use sample covariance
    if n == 1:
        rate_cov = _count_rate_and_cov(counts[0], live_times[0])[1]
    else:
        rate_cov = np.zeros((m, m))
        for _counts, _lt in zip(counts, live_times):
            rate = _counts / _lt
            rate_cov += np.outer(rate - rate_mean, rate - rate_mean)
        rate_cov /= n - 1

    return rate_mean, rate_median, rate_cov


class CountRateFilter(abc.ABC):
    """Base class for filters to apply to count rate data.

    The filter input will be the spectrum (units of counts per bin), the
    timestamp, and the livetime. The prediction will be the count rate
    (CPS per bin), and the covariance matrix of the estimated rate.

    Any filter should follow this usage pattern:
        (1) Instantiate the filter
            filter = CountRateFilter(**kwargs)
        (2) While not done:
            (2a) Update the filter with current data
                filter.update(spectrum, timestamp, live_time)
            (2b) Use filter to predict mean rate and its covariance at the
                 next sample.
                mean, cov = filter.predict(live_time_next)
        (3) If any large time gaps are encountered, filter.clear() may be
            used to reset the filter to defaults.
    """

    # named quantities sufficient to fully describe the filter state
    _attrs = []

    def __init__(self, **kwargs):
        """Initialize the filter."""
        # Check classname
        self.classname = type(self).__name__
        if "classname" in kwargs:
            assert kwargs.pop("classname") == self.classname
        # Initialize attributes to None
        for k in self._attrs:
            if not hasattr(self, k):
                try:
                    setattr(self, k, None)
                except AttributeError:
                    # Cannot set this attribute to None. Skipping
                    pass
        # Handle remaining kwargs as attributes
        for k, v in kwargs.items():
            assert k in self._attrs
            setattr(self, k, v)

    def clear(self):
        """Clear any buffers used by the filter and reset its state."""
        pass

    def update(self, spectrum, timestamp, live_time):
        """Update the filter with the current measurement.

        Parameters
        ----------
        spectrum : int or array_like
            The measured spectrum, either an integer or 1D array.
        timestamp : float
            The timestamp of the measurement.
        live_time : float
            The live time of the measurement.
        """
        pass

    @classmethod
    def from_dict(cls, dic: dict):
        """Create a filter from a dict."""
        return cls(**dic)

    def to_dict(self) -> dict:
        """Return the filter settings and state as a dictionary."""
        d = {}
        for k in self._attrs:
            d[k] = getattr(self, k)
        d["classname"] = type(self).__name__
        return d

    # ---------------------------- Abstract Methods ---------------------------

    @abc.abstractmethod
    def predict(self, live_time):
        """Predict the mean and covariance matrix of the next measurement.

        Parameters
        ----------
        live_time : float
            The live time of the next measurement. Used to scale the count
            rate and rate covariance to the measurement time.

        Returns
        -------
        mean : array_like
            The predicted mean of the spectrum, a 1D array.
        covariance_matrix : array_like
            The predicted covariance of the spectrum, a 2D array.
        """
        pass


def _get_countrate_classes():
    """Find the CountRateFilter classes in this module.

    Returns
    -------
    classes : dict
        Dictionary of CountRateFilter classes keyed by their names.
    """
    classes = {}
    for name, cls in inspect.getmembers(
        importlib.import_module("radai.filters"), inspect.isclass
    ):
        if issubclass(cls, CountRateFilter) and name != "CountRateFilter":
            classes[name] = cls
    return classes


def make_filter(arg):
    """Create filter from arg, if dict uses as kwargs to instantiate.

    Parameters
    ----------
    arg : Union[CountRateFilter, dict]
        A filter object (just returned) or the kwargs for the filter. If
        kwargs, they must contain a "classname" field giving a valid class
        from this module, and the remaining kwargs are passed to
        instantiate the filter.

    Returns
    -------
    CountRateFilter
        The desired filter instance.
    """
    if isinstance(arg, dict):
        arg = dict(**arg)
        classname = arg.pop("classname")
        classes = _get_countrate_classes()
        if classname not in classes:
            raise ValueError(
                f"Filter class {classname} not found in filters module\n"
                f"({list(classes.keys())})"
            )
        return classes[classname](**arg)
    elif isinstance(arg, CountRateFilter):
        return arg
    else:
        raise TypeError(f"Cannot make filter from {type(arg)}: {arg}")
