"""A static filter (fixed mean and covariance) for count rates."""

import numpy as np
from .filter import CountRateFilter, _count_rate_sample_mean_median_cov


class StaticFilter(CountRateFilter):
    """Hold a constant mean and covariance. Data can be vectors or scalars."""

    _attrs = ["mean", "cov"]

    def __init__(self, mean=0.0, cov=1.0, **kwargs):
        """Initialize the filter and check the parameters.

        Parameters
        ----------
        mean : float or array_like
            The mean of the data, either a float or 1D array.
        cov : float or array_like
            The covariance of the data, either a float or 2D array.
        """
        # check that the inputs arrays have the correct dimensions
        mean, cov = np.asarray(mean), np.asarray(cov)
        if mean.shape == ():
            assert cov.shape == ()
            mean.shape = (1,)
            cov.shape = (1, 1)
        assert len(mean.shape) == 1
        assert len(cov.shape) == 2
        assert mean.shape[0] == cov.shape[0] == cov.shape[0]
        # call base init
        kwargs["mean"] = mean
        kwargs["cov"] = cov
        super().__init__(**kwargs)

    @classmethod
    def from_data(cls, spectrum, live_times):
        """Calculate mean and covariance matrix of the count rates.

        Parameters
        ----------
        spectrum : array_like
            If 1D assume single feature and shape is (n_observations,).
            if 2D assume shape is (n_observations, n_bins).
        live_times : array_like
            The live times, a 1D array of length n_observations.

        Returns
        -------
        filter_instance : StaticFilter
            Filter instance with mean and covariance derived from the data.
        """
        mean, _, cov = _count_rate_sample_mean_median_cov(spectrum, live_times)
        return cls(mean=mean, cov=cov)

    def predict(self, live_time):
        """Return the constant mean and covariance matrix.

        Parameters
        ----------
        live_time : float
            The live time of the next measurement. Used to scale the count
            rate and rate covariance to the measurement time.

        Returns
        -------
        mean : array_like
            The predicted mean of the spectrum, a 1D array.
        covariance_matrix : array_like
            The predicted covariance of the spectrum, a 2D array.
        """
        # scale to the measurement live time
        mean = self.mean * live_time
        cov = self.cov * live_time**2
        return mean, cov
