"""A moving average filter for count rates."""

from .filter import CountRateFilter, _count_rate_sample_mean_median_cov
from ..buffer import SpectraBuffer


class MovingAverageFilter(CountRateFilter):
    """A filter that keeps of a buffer of the `n` most recent measurements.

    Prediction is the mean count rate per bin of the most recent `n`
    measurements, and the variance is the sample covariance matrix.

    When buffer length is 1 or if `n` == 1, will estimate the covariance
    matrix from the Poisson statistics of the single measurement.
    """

    _attrs = ["n", "buffer_spectra", "buffer_ts", "buffer_lt"]

    def __init__(
        self, n=100, buffer_spectra=None, buffer_ts=None, buffer_lt=None, **kwargs
    ):
        """Initialize the filter and check the parameters.

        Parameters
        ----------
        n : int
            The length of the buffer to use for the moving average.
        buffer_spectra : list
            A list of previous spectra.
        buffer_ts : list
            A list of timestamps of previous data.
        buffer_lt : list
            A list of live times of previous data.
        """
        # check attribute values and initialize as needed
        assert isinstance(n, int)
        assert n >= 1
        buffer_spectra = [] if buffer_spectra is None else buffer_spectra
        buffer_ts = [] if buffer_ts is None else buffer_ts
        buffer_lt = [] if buffer_lt is None else buffer_lt
        assert len(buffer_spectra) >= 0
        assert len(buffer_ts) >= 0
        assert len(buffer_lt) >= 0
        assert len(buffer_spectra) == len(
            buffer_ts
        ), f"{len(buffer_spectra)} != {len(buffer_ts)}"
        assert len(buffer_spectra) == len(
            buffer_lt
        ), f"{len(buffer_spectra)} != {len(buffer_lt)}"
        # Initialize buffer
        self.buffer = SpectraBuffer(n_time=n)
        for spec, ts, lt in zip(buffer_spectra, buffer_ts, buffer_lt):
            self.buffer.update(spec, ts, lt)
        # Call parent init without buffer data
        super().__init__(**kwargs)

    @property
    def n(self):
        return self.buffer.n_time

    @property
    def buffer_spectra(self):
        return [] if self.buffer.is_empty() else self.buffer.spectra.tolist()

    @property
    def buffer_ts(self):
        return [] if self.buffer.is_empty() else self.buffer.timestamps.tolist()

    @property
    def buffer_lt(self):
        return [] if self.buffer.is_empty() else self.buffer.live_times.tolist()

    def clear(self) -> None:
        """Clear all buffers."""
        self.buffer.clear()

    def update(self, spectrum, timestamp, live_time) -> None:
        """Update the filter with the current measurement.

        Parameters
        ----------
        spectrum : int or array_like
            The measured spectrum, either an integer or 1D array.
        timestamp : float
            The timestamp of the measurement.
        live_time : float
            The live time of the measurement.
        """
        self.buffer.update(spectrum, timestamp, live_time)

    def batch_update(self, spectra, timestamps, live_times) -> None:
        """Update the filter with the given measurements

        Parameters
        ----------
        spectra : array
            2D array of spectra with shape=(num_measurements, num_bins)
        timestamps : array
            1D array timestamps of type int or float with shape=(num_measurements)
        live_times : array
            1D array of live times of type float with shape=(num_measurements)
        """
        assert len(spectra) == len(timestamps)
        assert len(timestamps) == len(live_times)

        # serially update the filter with each given measurements
        # NOTE: There is likely a more efficient way to do this other than using
        #       a for loop
        for spectrum, timestamp, live_time in zip(spectra, timestamps, live_times):
            self.update(spectrum, timestamp, live_time)

    def predict(self, live_time):
        """Predict the mean and covariance matrix of the next measurement.

        Parameters
        ----------
        live_time : float
            The live time of the next measurement. Used to scale the count
            rate and rate covariance to the measurement time.

        Returns
        -------
        mean : array_like
            The predicted mean of the spectrum, a 1D array.
        covariance_matrix : array_like
            The predicted covariance of the spectrum, a 2D array.
        """
        if len(self.buffer) == 0:
            raise Exception(
                "Cannot predict until buffer contains at least 1 measurement"
            )
        # calculate sample mean and covariance from buffer
        mean, _, cov = _count_rate_sample_mean_median_cov(
            self.buffer.spectra,
            self.buffer.live_times,
        )
        assert mean.shape == (self.buffer.n_energy,)
        # scale to the measurement live time
        mean *= live_time
        cov *= live_time**2
        return mean, cov

    def is_ready(self) -> bool:
        """Alias for `is_full`

        Returns
        -------
        bool
            True if the data buffer is full, else false.
        """
        return self.is_full()

    def is_full(self) -> bool:
        """Check if the data buffer is full

        Returns
        -------
        bool
            True if the data buffer is full, else false.
        """
        return self.buffer.is_ready()
