from typing import Any, Iterable
from copy import deepcopy
import numpy as np
from pandas import Series, DataFrame
from .tools import is_close, SingleTypeList


class Result(object):
    """Result values from a single analysis (result of Algorithm.analyze).

    A Result is not a dictionary so the values can be accessed via the
    get_value method instead of subscripting with __getitem__.

    Attributes
    ----------
    origin : str
        The name of the creator of this result (algorithm name, etc)
    source_id : int
        Integer ID of the source. Two reservations (special values) exist:
            0: background
            -1: unknown
        Anything >=1 is an anomalous source. The source id to name is
        handled seperately.
    is_alarm : bool
        Is this result an alarm.
    alarm_metric : float
        The primary alarm metric.
    alarm_metric_name : str
        The name of the primary alarm metric (i.e. z-score, p-value, etc).
    over_threshold : bool
        Is the result metric over threshold (may not be an alarm).
    timestamp_start : float
        The unix epoch time of the beginning of the data which was used for
        this result.
    timestamp_stop : float
        The unix epoch time of the end of the data which was used for this
        result.
    timestamp_alarm : float
        The specific alarm time (typically sometime between the start and
        stop time).
    misc : dict
        User defined dictionary data associated with this analysis.
    """

    def __init__(
        self,
        origin: str,
        source_id: int,
        is_alarm: bool,
        alarm_metric: float,
        alarm_metric_name: str,
        timestamp_start: float,
        timestamp_stop: float,
        timestamp_alarm: float = None,
        over_threshold: bool = None,
        **misc: dict,
    ):
        """Create Result.

        Parameters
        ----------
        source_id : int
            Integer ID of the source. Two reservations (special values) exist:
                0: background
                -1: unknown
            Anything >=1 is an anomalous source. The source id to name is
            handled seperately.
        is_alarm : bool
            Is this result an alarm.
        alarm_metric : float
            The primary alarm metric.
        alarm_metric_name : str
            The name of the primary alarm metric (i.e. z-score, p-value, etc).
        timestamp_start : float
            The unix epoch time of the beginning of the data which was used for
            this result.
        timestamp_stop : float
            The unix epoch time of the end of the data which was used for this
            result.
        timestamp_alarm : float
            The specific alarm time (typically sometime between the start and
            stop time), by default the center of the start/stop time window.
        over_threshold : bool, optional
            Was the result metric over the alarm threshold? This may not have
            resulted in an alarm based on additional processing in the upstream
            algorithm. By default equal to `is_alarm`

        Other parameters
        ----------------
        misc : dict, optional
            User defined dictionary data associated with this result,
            by default a empty dictionary
        """

        self.origin = str(origin)
        self.source_id = int(source_id)
        self.is_alarm = bool(is_alarm)
        self.alarm_metric = float(alarm_metric)
        self.alarm_metric_name = str(alarm_metric_name)
        self.timestamp_start = float(timestamp_start)
        self.timestamp_stop = float(timestamp_stop)
        self.timestamp_alarm = (
            (self.timestamp_stop + self.timestamp_start) / 2.0
            if timestamp_alarm is None
            else float(timestamp_alarm)
        )
        self.over_threshold = (
            bool(is_alarm) if over_threshold is None else bool(over_threshold)
        )
        self.misc = misc
        if self.timestamp_start >= self.timestamp_stop:
            raise ValueError(
                f"timestamp_start({self.timestamp_start}) must "
                f"be less than timestamp_stop({self.timestamp_stop})"
            )
        if not (self.timestamp_start <= self.timestamp_alarm <= self.timestamp_stop):
            raise ValueError(
                f"timestamp_alarm({self.timestamp_alarm}) must be in "
                f"[timestamp_start({self.timestamp_start}), "
                f"timestamp_stop({self.timestamp_stop})]"
            )

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        for name in (
            "origin",
            "source_id",
            "is_alarm",
            "alarm_metric",
            "alarm_metric_name",
            "timestamp_start",
            "timestamp_stop",
            "timestamp_alarm",
            "over_threshold",
            "misc",
        ):
            if not is_close(getattr(self, name), getattr(other, name)):
                return False
        return True

    def __str__(self):
        return (
            f"{self.__class__.__name__}("
            f"    origin={self.origin}\n"
            f"    source_id={self.source_id}\n"
            f"    is_alarm={self.is_alarm}\n"
            f"    alarm_metric={self.alarm_metric}\n"
            f"    alarm_metric_name={self.alarm_metric_name}\n"
            f"    timestamp_start={self.timestamp_start}\n"
            f"    timestamp_stop={self.timestamp_stop}\n"
            f"    timestamp_alarm={self.timestamp_alarm}\n"
            f"    over_threshold={self.over_threshold}\n"
            f"    misc={self.misc}\n"
        )

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"origin={self.origin}, "
            f"source_id={self.source_id}, "
            f"is_alarm={self.is_alarm}, "
            f"alarm_metric={self.alarm_metric}, "
            f"alarm_metric_name={self.alarm_metric_name}, "
            f"timestamp_start={self.timestamp_start}, "
            f"timestamp_stop={self.timestamp_stop}, "
            f"timestamp_alarm={self.timestamp_alarm}, "
            f"over_threshold={self.over_threshold}, "
            f"{', '.join([f'{k}={v}' for k, v in self.misc.items()])}"
            f")"
        )

    def get_value(self, name: str) -> Any:
        """Get attribute value or misc dict value.

        Parameters
        ----------
        name : str
            Name of attribute or misc dict key.

        Returns
        -------
        Any
        """
        try:
            return getattr(self, name)
        except AttributeError:
            return self.misc[name]

    def get_duration(self) -> float:
        """Calculate the duration for the data in this analysis Result.

        Returns
        -------
        float
            Duration in seconds
        """
        return self.timestamp_stop - self.timestamp_start

    def copy(self):
        """Make a deep copy of every item and return a new object.

        Returns
        -------
        class(self)
            New object of this class with deepcopied items.
        """
        return deepcopy(self)

    def to_dict(self) -> dict:
        """Convert object to dictionary"""
        dic = {
            "origin": self.origin,
            "source_id": self.source_id,
            "is_alarm": self.is_alarm,
            "alarm_metric": self.alarm_metric,
            "alarm_metric_name": self.alarm_metric_name,
            "timestamp_start": self.timestamp_start,
            "timestamp_stop": self.timestamp_stop,
            "timestamp_alarm": self.timestamp_alarm,
            "over_threshold": self.over_threshold,
        }
        for k, v in self.misc.items():
            dic[k] = v
        return dic

    def to_series(self) -> Series:
        """Convert object to Pandas Series"""
        return Series(self.to_dict())


class Results(SingleTypeList):
    """A list-like collection of Result objects for different sources.

    No restrictions are placed on duplicate values of the internal Result(s).
    This means the items can be duplicate times, sources, etc.
    """

    ITEM_TYPE = Result
    UNIQUE_ITEM_ATTRIBUTES = frozenset()

    def __init__(self, timestamp: float, items: Iterable[Result] = None):
        """A list-like collection of Result objects for different sources.

        Parameters
        ----------
        timestamp : float
            The unix epoch time of these results (in seconds).
        items : Iterable[Result], optional
            Results to include at initialization.
        """
        self.timestamp = float(timestamp)
        super().__init__(items)

    def __eq__(self, other) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        if not np.isclose(self.timestamp - other.timestamp, 0.0):
            return False
        for r0, r1 in zip(self, other):
            if r0 != r1:
                return False
        return True

    def __copy__(self):
        return self.copy()

    def __deepcopy__(self, memo):
        return self.copy()

    def copy(self):
        """Make a deep copy of every item and return a new object.

        Returns
        -------
        class(self)
            New object of this class with deepcopied items.
        """
        return self.__class__(timestamp=self.timestamp, items=[r.copy() for r in self])

    def sort(self, name: str, reverse: bool = False) -> None:
        """Sort items in-place by the values for the named attribute.

        Parameters
        ----------
        name : str
            Result attribute name
        """
        self.data.sort(key=lambda result: result.get_value(name), reverse=reverse)

    def contains_alarm(self) -> bool:
        """Do these Results contain one of more alarm? Empty Results cannot
        contain an alarm.

        Returns
        -------
        bool
        """
        return any([r.is_alarm for r in self])

    def to_dataframe(self) -> DataFrame:
        """Convert list to Pandas DataFrame"""
        return DataFrame([r.to_dict() for r in self])


class ResultsBatch(SingleTypeList):
    """A list-like collection of Results objects."""

    ITEM_TYPE = Results
    UNIQUE_ITEM_ATTRIBUTES = frozenset()

    def get_all(self, name: str, results_index: int = None) -> list:
        """Get all values for name in each result.

        Parameters
        ----------
        name : str
            Result attribute.
        results_index : int, optional
            Index of Result in each Results object to get the value, by default
            get values from every Result (all indices)

        Returns
        -------
        list
            Result values.
        """
        values = []
        for results in self:
            if results_index is None:
                for result in results:
                    values.append(result.get_value(name))
            else:
                values.append(results[results_index].get_value(name))
        return values

    def to_dataframe(self) -> DataFrame:
        """Convert list to Pandas DataFrame"""
        dcts = []
        for rs in self:
            for r in rs:
                dcts.append(r.to_dict())
        return DataFrame(dcts)
