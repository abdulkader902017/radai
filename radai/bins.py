"""Binning tools and objects.
"""
from .core import array_like, scalar_like
from .configuration import Configuration
import numpy as np


def centers_from_edges(bin_edges):
    return (bin_edges[:-1] + bin_edges[1:]) / 2.0


def edges_from_centers(bin_centers):
    bin_edges = np.zeros(len(bin_centers) + 1)
    bin_edges[1:-1] = (bin_centers[:-1] + bin_centers[1:]) / 2.0
    bin_edges[0] = bin_centers[0] - (bin_centers[1] - bin_centers[0]) / 2.0
    bin_edges[-1] = bin_centers[-1] + (bin_centers[-1] - bin_centers[-2]) / 2.0
    return bin_edges


def check_bins(num_bins, bin_edges, bin_centers):
    """check bin parameters for consistency

    Parameters
    ----------
    num_bins : int
        The number of bins in expected spectra.
    bin_edges : array_like or None
        The edges of the bins in keV -- (num_bins + 1)
    bin _centers : array_like or None
        The centers of the bins in keV -- (num_bins)

    Returns
    -------
    num_bins : int
        The number of bins in expected spectra.
    bin_edges : array_like or None
        As provided or estimated from bin_centers
    bin_centers : array_like or None
        As provided or estimated from bin_edges
    """

    if bin_edges is not None:
        if bin_centers is not None:
            assert len(bin_centers) == len(bin_edges) - 1
            assert np.all(bin_edges[1:] > bin_centers)
            assert np.all(bin_edges[:-1] < bin_centers)
        else:
            bin_centers = centers_from_edges(bin_edges)
        if num_bins is None:
            num_bins = len(bin_centers)
        else:
            assert num_bins == len(bin_centers)
        return num_bins, bin_edges, bin_centers

    elif bin_centers is not None:
        bin_edges = edges_from_centers(bin_centers)
        if num_bins is None:
            num_bins = len(bin_centers)
        else:
            assert num_bins == len(bin_centers)
        return num_bins, bin_edges, bin_centers
    elif num_bins is not None:
        # Could set bin_edges and bin_centers here but lets leave them None
        pass
    else:
        raise ValueError("Need to provide num_bins, bin_edges, or bin_centers")

    return num_bins, bin_edges, bin_centers


def powerlaw_bins(x0, x1, nbins=128, alpha=0.5):
    """Create bins with widths approximately proportional to x^alpha.

    These bins are formed by approximating the difference equation
        x_{j+1} - x_j = K (x_j)^a

    with the differential equation
        dx/dj = K x^a

    This equation can be solved by separation of variables:
        x^{-a} dx = K dj

    For a != 1:
        x^{1-a} = K j + C
        (x_0)^{1-a} = C
        (x_1)^{1-a} = C + K N
            ==> K = [(x_1)^{1-a} - (x_0)^{1-a}] / N
    Therefore:
        (x_j)^{1-a} = (x_0)^{1-a} + [(x_1)^{1-a} - (x_0)^{1-a}] (j/N)

    For a == 1:
        ln(x) = K j + C
        ln(x_0) = C
        ln(x_1) = C + K N
            ==> K = [ln(x_1) - ln(x_0)] / N
    Therefore:
        ln(x_j) = ln(x_0) + [ln(x_1) - ln(x_0)] (j/N)
    """

    # check arguments
    assert x0 >= 0
    assert x1 > x0
    assert nbins >= 1
    assert alpha >= 0
    # make the bins
    if not np.isclose(alpha, 1):
        p = 1.0 - alpha
        bin_edges = np.linspace(x0**p, x1**p, nbins + 1)
        bin_edges = pow(bin_edges, 1 / p)
    else:
        bin_edges = np.linspace(np.log(x0), np.log(x1), nbins + 1)
        bin_edges = np.exp(bin_edges)
    # sanity checks on bins
    assert np.isclose(bin_edges[0], x0)
    assert np.isclose(bin_edges[-1], x1)
    assert len(bin_edges) == nbins + 1
    bin_centers = (bin_edges[1:] + bin_edges[:-1]) / 2
    return bin_edges, bin_centers


class Bins(Configuration):
    def __init__(self, num=None, edges=None, centers=None, **kwargs):

        num, edges, centers = check_bins(
            num_bins=num, bin_edges=edges, bin_centers=centers
        )
        if edges is not None:
            super().__init__(num=num, edges=edges, centers=centers, **kwargs)
        else:
            super().__init__(num=num)

    @classmethod
    def from_linear(cls, num, low_edge, high_edge):
        edges = np.linspace(low_edge, high_edge, num + 1)

        return cls(
            num=num, edges=edges, centers=None, low_edge=low_edge, high_edge=high_edge
        )

    @classmethod
    def from_power(cls, num, low_edge, high_edge, alpha):
        edges, centers = powerlaw_bins(low_edge, high_edge, num, alpha=alpha)

        return cls(
            num=num,
            edges=edges,
            centers=None,
            low_edge=low_edge,
            high_edge=high_edge,
            alpha=alpha,
        )

    def __setitem__(self, key, value):
        if not (
            isinstance(value, array_like)
            or isinstance(value, scalar_like)
            or value is None
        ):
            raise ValueError("Only scalars or arrays in Bins.")
        super(Bins, self).__setitem__(key, value)

    def __len__(self):
        return self.num

    def read(cls, filename):
        raise NotImplementedError

    def write(self, filename):
        raise NotImplementedError
