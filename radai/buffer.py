"""Buffer tools, in particular a spectra buffer."""
import numpy as np


class SpectraBuffer:
    """Buffer containing counts (1D or 2D), timestamps (1D) and live times(1D)

    The count array has shape [t, e] with a 1 length second dimension if
    buffering counts. The -1 time index contains the most recent data such that
    the first dimension of each array is time sorted.
    """

    def __init__(
        self,
        n_time: int,
        n_energy: int = None,
        spectra_dtype=np.uint32,
    ) -> None:
        self._n_time = int(n_time)
        self._counter = 0
        self._spectra_dtype = np.dtype(spectra_dtype)
        self._n_energy = None
        self._spectra = None
        self._timestamps = np.full((self.n_time,), np.nan, dtype=np.float64)
        self._live_times = np.full((self.n_time,), np.nan, dtype=np.float32)
        if n_energy is not None:
            self._set_n_energy(n_energy)

    def __len__(self) -> int:
        return self.n_time

    def __iter__(self) -> tuple:
        """Iterate rows of spectra, timestamps and live times starting with recent

        Returns
        -------
        tuple
            spectra, timestamp, live time

        Yields
        -------
        Iterator[tuple]
            Generator yielding tuples of spectra, timestamp, live time starting
            with the most recent data.
        """
        self.check_ready()
        for s, t, l in zip(self.spectra, self.timestamps, self.live_times):
            yield s, t, l

    def __getitem__(self, idx) -> tuple:
        return self.get_spectra(idx), self.get_timestamps(idx), self.get_live_times(idx)

    def _set_n_energy(self, n_energy: int) -> None:
        self._n_energy = int(n_energy)
        self._spectra = np.zeros(
            (self.n_time, self.n_energy), dtype=self._spectra_dtype
        )

    @property
    def n_time(self):
        return self._n_time

    @property
    def n_energy(self):
        return self._n_energy

    @property
    def spectra(self):
        return self._spectra

    @property
    def timestamps(self):
        return self._timestamps

    @property
    def live_times(self):
        return self._live_times

    @property
    def counter(self) -> int:
        return self._counter

    def get_spectra(self, idx=Ellipsis) -> np.ndarray:
        """Get all the spectra or a subset.

        Parameters
        ----------
        idx : int or slice or array[bool] or array[int], optional
            Slice operator for the spectra array, by default Ellipsis (all data)

        Returns
        -------
        array
            Single spectrum (1D) or spectra (2D).
        """
        self.check_ready()
        return self.spectra[idx]

    def get_timestamps(self, idx=Ellipsis):
        """Get all the timestamps or a subset.

        Parameters
        ----------
        idx : int or slice or array[bool] or array[int], optional
            Slice operator for the timestamp array, by default Ellipsis (all data)

        Returns
        -------
        float or array[float]
            Single or array of timestamps.
        """
        self.check_ready()
        return self.timestamps[idx]

    def get_live_times(self, idx=Ellipsis):
        """Get all the live times or a subset.

        Parameters
        ----------
        idx : int or slice or array[bool] or array[int], optional
            Slice operator for the live time array, by default Ellipsis (all data)

        Returns
        -------
        float or array[float]
            Single or array of live times.
        """
        self.check_ready()
        return self.live_times[idx]

    def is_empty(self) -> bool:
        """Does the buffer have any data?

        This also means that the spectra array is initialized.

        Returns
        -------
        bool
            If the buffer has data.
        """
        return self._counter == 0

    def is_ready(self) -> bool:
        """Does the buffer have enough data for analysis (is it full)?

        Returns
        -------
        bool
            If the buffer is ready.
        """
        return self._counter >= self.n_time

    def check_ready(self) -> None:
        """Check that that buffer is ready for analysis (full).

        Raises
        ------
        ValueError
            If the buffer doesn't have enough data yet.

        Notes
        -----
        See `is_ready` for more details.
        """
        if not self.is_ready():
            raise ValueError(
                f"{self.__class__} does not have {self.n_time} values "
                "or n_energy has not been set. Try updating the buffer!"
            )

    def update(
        self, spectrum: np.ndarray, timestamp: float = np.nan, live_time: float = np.nan
    ) -> None:
        """Update the buffers with this recent measurement.

        Parameters
        ----------
        spectrum : np.ndarray
            Single 1D spectrum of length n_energy
        timestamp : float, optional
            POSIX timestamp of the end of the measurement in seconds,
            by default np.nan
        live_time : float, optional
            Live time of the measurement in seconds, by default np.nan

        Raises
        ------
        ValueError
            # If a mix of nan and float timestamps are added.
            # If a mix of nan and float live times are added.
            If a timestamp before the most recent timestamp is added.

        Notes
        -----
        The buffers do not increment with np.roll because this will make a
        memory copy.
        """
        spectrum = np.ravel(spectrum)
        timestamp = np.nan if timestamp is None else float(timestamp)
        live_time = np.nan if live_time is None else float(live_time)
        if self.n_energy is None:
            self._set_n_energy(spectrum.shape[0])
        # TODO: remove these checks
        # if np.isnan(timestamp) is not np.isnan(self.timestamps[0]):
        #     raise ValueError("Cannot mix nan and float timestamps!")
        # if np.isnan(live_time) is not np.isnan(self.live_times[0]):
        #     raise ValueError("Cannot mix nan and float live times!")
        # NOTE: this only checks the most recent timestamp.
        if not np.isnan(timestamp) and not np.isnan(self.timestamps[0]):
            if self.timestamps[-1] >= timestamp:
                raise ValueError(
                    "Attempting to add a timestamp not in the future. "
                    f"Most recent timestamp: {self.timestamps[-1]} "
                    f"vs trying to add: {timestamp}"
                )
        for item, buffer in (
            (spectrum, self.spectra),
            (timestamp, self.timestamps),
            (live_time, self.live_times),
        ):
            buffer[:-1] = buffer[1:]
            buffer[-1] = item
        self._counter += 1

    def fill(
        self,
        spectra: np.ndarray,
        timestamps: np.ndarray = np.nan,
        live_times: np.ndarray = np.nan,
    ) -> None:
        """Fill the buffers with the input data.

        Parameters
        ----------
        spectra : np.ndarray
            Spectra data array (n_time, e_energy) or scalar
        timestamps : np.ndarray, optional
            Timestamps as array (n_time, ) or scalar, by default np.nan
        live_times : np.ndarray, optional
            Live times as array (n_time, ) or scalar, by default np.nan
        """
        if not np.isscalar(timestamps) and np.any(np.diff(timestamps) < 0):
            raise ValueError(
                f"Timestamps are not monotonically increasing: {timestamps}"
            )
        self.spectra[...] = spectra
        self.timestamps[...] = timestamps
        self.live_times[...] = live_times

    def clear(self) -> None:
        """Empty the buffers and unset the number of energy bins."""
        self._spectra = None
        self._n_energy = None
        self._timestamps[...] = np.nan
        self._live_times[...] = np.nan
        self._counter = 0
