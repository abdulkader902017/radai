"""Standalone utility to fit NMF solution."""

import numpy as np
from .nmf_regularizer import make_nmf_regularizer


_ZERO = 1e-9


def factorize_nmf(
    X,
    n_comps_new,
    A=None,
    V=None,
    freeze_V=True,
    eps=1e-6,
    regularizer=None,
    verbose=False,
):
    """Fit the data X with an NMF model (X = A @ V) minimizing Poisson loss.

    Applies the well-known multiplicative update rules from [1] with random
    initialization of A and V, unless they're given.

    [1] Lee, D., Seung, H., "Learning the parts of objects by non-negative
        matrix factorization," Nature 401, 788–791 (1999).
        https://doi.org/10.1038/44565

    Parameters
    ----------
    X : array_like
        Numpy array of count data (n_measurements, n_bins).
    n_comps_new : int
        The number of components to fit, or number of *additional* components
        to find if V is given. Can be zero if V is provided; in that case
        will fit (or re-fit) the weights matrix A and add no additional
        components.
    A : array_like, optional
        Existing NMF weights (n_measurements, n_comps_old). These weights will
        be used to intialize the fit of the first `n_comps_old` components
        when fitting the additional `n_comps_new` new components.
        By default None.
    V : array_like, optional
        Existing NMF components (n_comps_old, n_bins). These components will
        be frozen depending on freeze_V and `n_comps_new` additional components
        will be found.
        By default None.
    freeze_V : bool, optional
        Whether to freeze the existing NMF components V (if V is given). If set
        to False, the given V will be used as a starting point and then change
        throughout the fit.
        By default True.
    eps : float, optional
        Change in loss function divided by measurement at which iterations
        will stop. By default 1e-6.
    regularizer : list, optional
        list of dicts containing parameters for NMF_Regularizer(s) or
        list of NMF_Regularizers, that will be applied during weight updates.
    verbose : bool, optional
        If True, will print the values of the loss. By default False.

    Returns
    -------
    A : array_like
        NMF weights for the new model (n_meas., n_comps_old + n_comps_new).
    V : array_like
        NMF components for the new model (n_comps_old + n_comps_new, n_bins).
    loss : float
        Negative log likelihood of final fit.
    """
    # check data (X)
    X = np.asarray(X)
    assert len(X.shape) == 2
    if n_comps_new > 0:
        assert X.shape[0] > X.shape[1]
    assert (X >= 0).all()
    n_meas, n_bins = X.shape

    # initialize components (V)
    if V is None:
        n_comps_old = 0
        n_comps = n_comps_new
        V2 = np.random.uniform(1e-1, 1e0, size=(n_comps, n_bins))
    else:
        V = np.asarray(V)
        assert len(V.shape) == 2
        n_comps_old = V.shape[0]
        assert n_comps_old >= 1
        assert V.shape[1] == n_bins
        assert (V >= 0).all()
        n_comps = n_comps_old + n_comps_new
        V2 = np.random.uniform(1e-1, 1e0, size=(n_comps, n_bins))
        V2[:n_comps_old, :] = V[:, :]
    # clip and normalize
    V2 = V2.clip(_ZERO)
    V2 /= V2.sum(axis=1)[:, None]
    frozen = np.zeros(n_comps, dtype=bool)
    frozen[:n_comps_old] = freeze_V

    assert n_comps >= 1, "NMF model must have at least one component"

    # initialize regularizer(s)
    regs = []
    if regularizer is not None:
        assert isinstance(regularizer, list)
        for item in regularizer:
            regs.append(make_nmf_regularizer(item, frozen))

    # initialize weights (A)
    A2 = np.random.uniform(1e-1, 1e0, size=(n_meas, n_comps))
    if A is not None:
        A = np.asarray(A)
        assert len(A.shape) == 2
        assert A.shape[0] == n_meas
        assert A.shape[1] == n_comps_old
        assert (A >= 0).all()
        A2[:, :n_comps_old] = A[:, :]
    elif len(regs) > 0:
        for reg in regs:
            # if the regularizer has a mean use it for initialization
            if hasattr(reg, "mean"):
                assert len(reg.mean.shape) == 2
                assert reg.mean.shape[0] == n_meas
                assert reg.mean.shape[1] == n_comps_old
                assert (reg.mean >= 0).all()
                A2[:, :n_comps_old] = reg.mean[:, :]
    else:
        pass

    A2 = A2.clip(_ZERO)

    # check number of constraints from the data and number of free parameters
    n_data = n_meas * n_bins
    n_free = n_meas * n_comps + n_comps_new * (n_bins - 1)
    if n_data <= n_free:
        raise ValueError(
            f"Model has {n_free} parameters but only {n_data} measurements"
        )

    # solve NMF equation X = A @ V to find source component
    assert eps > 0
    # calculate initial loss
    Xfit = A2 @ V2
    ones = np.ones_like(Xfit)
    reg_loss = 0
    # NOTE -- If regularizers for V are added this will need to be revised
    if len(regs) > 0:
        for reg in regs:
            reg_loss += reg.loss(A2)

    loss0 = (Xfit - X * np.log(Xfit)).sum() + reg_loss

    # iterate until tolerance met
    step = 0
    loss1 = loss0 + 1e6
    while np.abs((loss0 - loss1) / n_meas) > eps:
        loss0 = loss1
        if not frozen.all():
            # update non-frozen components only
            V2[~frozen, :] = (V2 * ((A2.T @ (X / Xfit)) / (A2.T @ ones)))[~frozen, :]
            # normalize components
            V2 = V2 / V2.sum(axis=1)[:, None]
            V2 = V2.clip(_ZERO)
        # update weights
        # NOTE -- If regularizers for V are added this will need to be revised.
        if len(regs) > 0:
            grad_neg = np.zeros_like(A2)
            grad_pos = np.zeros_like(A2)
            for reg in regs:
                grad_neg += reg.grad_neg(A2)
                grad_pos += reg.grad_pos(A2)
        else:
            grad_neg = 0
            grad_pos = 0
        A2 = A2 * (((X / Xfit) @ V2.T + grad_neg) / (ones @ V2.T + grad_pos))
        A2 = A2.clip(_ZERO)
        # calculate new loss
        Xfit = A2 @ V2
        reg_loss = 0
        # NOTE -- If regularizers for V are added this will need to be revised
        if len(regs) > 0:
            for reg in regs:
                reg_loss += reg.loss(A2)
        loss1 = (Xfit - X * np.log(Xfit)).sum() + reg_loss
        if verbose:
            print(
                f"step {step:6d} | loss: {loss1 / n_meas:.12f} | "
                f"change: {(loss0 - loss1) / n_meas:.12f}"
            )
        step += 1
    loss = (Xfit - X * np.log(Xfit)).sum()
    for reg in regs:
        loss += reg.loss(A2)
    return A2, V2, loss


def train_bkg_sources(
    spectra,
    spectra_ids,
    src_ids=None,
    n_bkg=1,
    n_src=1,
    eps=1e-6,
    sparsity=0.0,
    verbose=False,
):
    """Fit the spectra with NMF models to find background and source compoents.

    If `sparsity` is nonzero, applies a L_1/2-based regularization term to
    the new component weights and adjusts the multiplicative update rules
    to minimize the additional penalty. See the documentation for the
    `NMF_Regularizer` `Sparsity` subclass.

    Parameters
    ----------
    spectra : array_like
        Training gamma-ray data (num_measurements, num_bins).
    spectra_ids : array_like
        Integer label for the training set, length num_measurements.
    src_ids : array_like, optional
        The integer labels for the sources. By default use a sorted list of
        any IDs in `spectra_ids` that are not 0.
    n_bkg : int, optional
        The number of components to use for the background model. By default 1.
    n_src : int or dict, optional
        The number of additional components to use for each source model.
        If different numbers of components are desired for the different
        source models, can be a dictionary keyed by source ID. By default 1.
    eps : float, optional
        Change in loss function divided by measurement at which iterations
        will stop. By default 1e-6.
    sparsity : float, optional
        If positive, will calculate an L_1/2 norm on the source component
        weights only, with `sparsity` as the norm's coefficient in the loss.
        Must be very small or the modified multiplicative update rule can
        cause the loss to increase instead of decrease. By default 0.
    verbose : bool, optional
        If True, will print the values of the loss. By default False.

    Returns
    -------
    V_bkg : array_like
        The NMF components for the background.
    V_src : dict
        Dictionary of NMF source components keyed by source ID.
    """
    # check inputs
    spectra = np.asarray(spectra)
    n_meas, n_bins = spectra.shape
    assert n_meas > n_bins
    spectra_ids = np.asarray(spectra_ids)
    assert len(spectra_ids) == n_meas

    # make sure background and source IDs are consistent
    all_ids = sorted(np.unique(spectra_ids))
    assert 0 in all_ids
    if src_ids is None:
        src_ids = sorted([src_id for src_id in np.unique(spectra_ids) if src_id != 0])
    assert len(src_ids) >= 1
    for src_id in src_ids:
        assert src_id in all_ids
        assert src_id != 0

    # check numbers of background and source components requested
    assert n_bkg >= 1
    if isinstance(n_src, int):
        assert n_src >= 1
        n_src = {src_id: n_src for src_id in src_ids}
    for src_id in n_src.keys():
        assert src_id in src_ids
        assert n_src[src_id] >= 1

    # train background model
    A_bkg, V_bkg, _ = factorize_nmf(
        spectra[spectra_ids == 0], n_bkg, eps=eps, verbose=verbose
    )

    # run main loop
    V_src = {}
    regularizer = None
    if sparsity > 0:
        regularizer = [{"classname": "Sparsity", "coeff": sparsity}]
    for src_id in src_ids:
        A_s, V_s, _ = factorize_nmf(
            spectra[spectra_ids == src_id],
            n_src[src_id],
            V=V_bkg,
            eps=eps,
            regularizer=regularizer,
            verbose=verbose,
        )
        V_src[src_id] = np.array(V_s[n_bkg:, :])

    return V_bkg, V_src
