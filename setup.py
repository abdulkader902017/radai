#!/usr/bin/env python
"""radai: Python tools for radiological anomaly detection and identification"""
import sys
import site
from setuptools import setup, find_packages
import importlib.util

# Enables --editable install with --user
# https://github.com/pypa/pip/issues/7953
site.ENABLE_USER_SITE = "--user" in sys.argv[1:]

_spec = importlib.util.spec_from_file_location(
    "__metadata__", "./radai/__metadata__.py"
)
METADATA = importlib.util.module_from_spec(_spec)
_spec.loader.exec_module(METADATA)

with open("README.md", "r") as f:
    LONG_DESCRIPTION = f.read().split("## LICENSE")[0]

with open("requirements.txt", "r") as fh:
    REQUIREMENTS = [_line for _line in fh.readlines() if _line]

setup(
    name=METADATA.__name__,
    version=METADATA.__version__,
    description=METADATA.__description__,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url=METADATA.__url__,
    download_url=f"{METADATA.__url__}/-/tags",
    maintainer=METADATA.__maintainer__,
    maintainer_email=METADATA.__email__,
    classifiers=METADATA.__classifiers__,
    platforms="any",
    packages=find_packages(where="."),
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=REQUIREMENTS,
    setup_requires=["pytest-runner"],
    tests_require=["pytest", "pytest-cov"],
    extras_require={"full": ["fast-histogram", "tensorflow>2.0"]},
    license="Other/Proprietary License (see LICENSE.txt)",
)
