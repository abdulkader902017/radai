"""Test radai.algorithms.sad.SAD."""

import numpy as np
from radai.algorithms import SAD
from radai.spectrum_generator import RandomSpectrumGenerator


def test_sad():
    """Test the basic functionality of SAD."""
    # generate background data and train the algorithm
    int_time = 1.0
    spectrum = 30 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        bkg_rates=[1500.0],
        batch_size=1000,
    )
    alg = SAD(
        int_time=int_time,
        bin_edges=bin_edges,
        bin_centers=bin_centers,
        num_bins=len(bin_centers),
    )
    # HACK: set the threshold by hand
    alg.threshold = 100.0
    train_spectra, _, _, train_lts = gen.next()
    alg.fit(train_spectra, live_times=train_lts)

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, _, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, _, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
