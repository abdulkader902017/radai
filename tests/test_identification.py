"""Test radai.algorithms that identify spectra."""

import matplotlib.pyplot as plt
import pytest
from common import TEST_OUTPUTS_PATH
from test_all_algorithms import (
    generate_dataset,
    init_algorithm,
    train_algorithm,
    plot_metric_time,
    plot_src_ids_time,
)

ID_ALGORITHMS = [
    "multiplexedcew",
]


@pytest.mark.parametrize("alg_name", ID_ALGORITHMS)
def test_identification(alg_name):
    """Test the algorithms on spectra that are a constant rate and shape."""
    prefix = f"test_identification__{alg_name}__"
    # get data and train algorithm
    dset_name = "bkg_complex_src_multiple"
    data = generate_dataset(dset_name)
    alg = init_algorithm(alg_name, data["bin_edges"])[0]
    alg, train_info = train_algorithm(dset_name, data, alg_name, alg)

    # evaluate the algorithm on testing data
    ts = data["test_tss"][:5000]
    test_labels = data["test_labels"][: len(ts)]
    batch_results = alg.analyze_batch(
        data["test_spectra"][: len(ts)],
        data["test_tss"][: len(ts)],
        live_times=data["test_lts"][: len(ts)],
    )

    # pull out highest metric and corresponding source ID for each measurement
    alarm_metrics = []
    src_ids = []
    for results in batch_results:
        results.sort("alarm_metric", reverse=True)
        result = results[0]
        alarm_metrics.append(result.alarm_metric)
        if result.is_alarm:
            src_ids.append(result.source_id)
        else:
            src_ids.append(0)

    thresh = alg.threshold
    if alg_name == "multiplexedcew":
        # pull out the threshold from one of the sub-algorithms
        thresh = alg.sub_alg[1].threshold

    # plot the results
    plt.figure(figsize=(10, 3))
    plt.title(alg_name)
    plot_metric_time(ts, alarm_metrics, thresh)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}metric.png")
    plt.close()

    plt.figure(figsize=(10, 3))
    plt.title(alg_name)
    plot_src_ids_time(ts, src_ids, label=alg_name)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}srcids.png")
    plt.close()

    plt.figure(figsize=(10, 3))
    plt.title(alg_name)
    plot_src_ids_time(ts, test_labels, color="b", label="true IDs")
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}srcids_true.png")
    plt.close()
