import tempfile
from copy import deepcopy
import pytest
import numpy as np
from radai.configuration import Configuration, dict_like_is_equal


_mixed_seq = [0, 1.0, False, "a"]


@pytest.fixture(
    scope="function",
    params=[
        dict(
            integer_zero=0,
            float_one=1.0,
            boolean_true=True,
            boolean_false=False,
            string_a="a",
            string_b_cap="B",
            list_mixed=list(_mixed_seq),
            tuple_mixed=tuple(_mixed_seq),
            set_mixed=set(_mixed_seq),
            nested_dict={
                "nested": {
                    "nested": {
                        "nested": {
                            "integer_one": 1,
                            "float_two": 2.0,
                            "mixed_seq": _mixed_seq,
                        }
                    }
                }
            },
            nested_sequence=[
                0,
                1.0,
                True,
                "a",
                list(_mixed_seq),
                tuple(_mixed_seq),
                set(_mixed_seq),
                [[[tuple(_mixed_seq)]]],
                [np.arange(10)],
                tuple([[np.zeros(10, dtype=bool)]]),
            ],
            numpy_int_array=np.arange(10, dtype=int),
            numpy_float_array=np.arange(10, dtype=float),
            numpy_bool_array=np.zeros(10, dtype=bool),
        )
    ],
)
def config_kwargs(request):
    yield request.param


@pytest.fixture
def config(config_kwargs):
    return Configuration(**config_kwargs)


def test_dict_like(config_kwargs):
    c = Configuration(**config_kwargs)
    assert dict_like_is_equal(c, config_kwargs)


def test_null():
    c = Configuration()
    assert len(c) == 0


def test_eq(config):
    assert config == deepcopy(config)


def test_str(config):
    str(config)


def test_repr(config):
    repr(config)


@pytest.mark.parametrize("suffix", [".yml", ".yaml", ".json", ".p", ".pkl"])
def test_io(suffix, config):
    with tempfile.NamedTemporaryFile(suffix=suffix, delete=True) as tmp:
        config.write(tmp.name)
        config_read = Configuration.read(tmp.name)
        assert config == config_read
