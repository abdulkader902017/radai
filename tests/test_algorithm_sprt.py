"""Test radai.algorithms.SPRT"""

import numpy as np

from radai.algorithms import SPRT
from radai.filters import MovingAverageFilter
from radai.spectrum_generator import RandomSpectrumGenerator


def test_gross_counts():
    """Test the basic functionality of SPRT."""
    integration_time = 1.0
    alg = SPRT(
        MovingAverageFilter(),
        int_time=integration_time,
        alpha_0=0.001,
        beta_0=0.001,
        a=50.0,
        auto_calculate_thresholds=True,
    )
    # generate background data and train the algorithm
    spectrum = np.ones(100)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=integration_time,
        bkg_rates=[1500.0],
        n_total=5000,
        src_rates=[3000.0],
        src_spectra=[spectrum],
        n_bkg=1000,
    )
    counts = []
    tstamps = []
    alarms = []
    metrics = []
    for spec, _, tstamp, lt in gen:
        counts.append(spec.sum())
        tstamps.append(tstamp)
        results = alg.analyze(spec, tstamp, live_time=lt)
        if len(results) > 0:
            alarms.append(results[0].is_alarm)
            metrics.append(results[0].alarm_metric)
        else:
            alarms.append(0.0)
            metrics.append(False)

    return tstamps, counts, alarms, metrics, alg.a


def test_all_ready_with_filter():
    integration_time = 1.0
    alg = SPRT(
        MovingAverageFilter(),
        int_time=integration_time,
        alpha_0=0.001,
        beta_0=0.001,
        a=50.0,
        auto_calculate_thresholds=True,
    )

    # generate background data
    spectrum = np.ones(100)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=integration_time,
        bkg_rates=[1500.0],
        n_total=5000,
        src_rates=[3000.0],
        src_spectra=[spectrum],
        n_bkg=1000,
    )

    # the algorithm should NOT be ready
    assert not alg._all_ready("bkg_filter")
    assert not alg._all_ready(alg.bkg_filter)
    assert not alg.is_ready()

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = alg.bkg_filter.n
    test_spectra, _, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
    assert alg._all_ready("bkg_filter")
    assert alg._all_ready(alg.bkg_filter)
    assert alg.is_ready()


if __name__ == "__main__":
    import matplotlib

    matplotlib.use("TkAgg")
    import matplotlib.pyplot as plt

    tstamps, counts, alarms, metrics, thr = test_gross_counts()
    fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True)
    ax[0].step(tstamps, counts, color="k")
    ax[0].set_ylabel("Counts")
    ax[1].step(tstamps, alarms, color="k")
    ax[1].set_ylabel("Alarms")
    ax[2].step(tstamps, metrics, color="k")
    ax[2].axhline(y=thr, color="r")
    ax[2].set_ylabel("Metrics")
    ax[2].set_xlabel("Timestamps")
    plt.show()
