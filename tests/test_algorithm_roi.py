"""Test radai.algorithms.ROI."""

import numpy as np
from radai.algorithms import ROI
from radai.spectrum_generator import RandomSpectrumGenerator


def test_roi():
    """Test the basic functionality of ROI algorithm."""
    # generate background data and train the algorithm
    int_time = 1.0
    spectrum = 30 * np.ones(100)
    bin_edges = np.arange(len(spectrum) + 1)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        bkg_rates=[1500.0],
        batch_size=1000,
    )
    alg = ROI(
        int_time=int_time,
        bin_edges=bin_edges,
        src_regions=[[30, 40]],
        bkg_regions=[[10, 30], [50, 70]],
    )

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 1.0

    train_spectra, _, _, train_lts = gen.next()
    spectra_ids = np.zeros(len(train_lts), dtype=int)
    alg.fit(train_spectra, spectra_ids)

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, _, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, _, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
