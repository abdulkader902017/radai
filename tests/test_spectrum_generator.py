"""Test radai.spectrum_generator classes."""

import matplotlib.pyplot as plt
import numpy as np
import pytest
from radai.spectrum_generator import RandomSpectrumGenerator
from radai.tools import hist_outline
from common import TEST_OUTPUTS_PATH


# set up test background and source spectra
BKG_SPECTRUM0 = 30 * np.arange(1, 21)[::-1] / 20.0
BKG_SPECTRUM1 = 30 * np.arange(1, 21) / 20.0
SRC0 = np.zeros_like(BKG_SPECTRUM0)
SRC0[5:10] = 20
SRC1 = np.zeros_like(BKG_SPECTRUM0)
SRC1[12:17] = 20

BKG_SPECTRA1 = [BKG_SPECTRUM0]
BKG_SPECTRA2 = [BKG_SPECTRUM0, BKG_SPECTRUM1]
SRC_SPECTRA0 = []
SRC_SPECTRA1 = [SRC0]
SRC_SPECTRA2 = [SRC0, SRC1]
BIN_EDGES = np.linspace(0, 3000, num=len(BKG_SPECTRUM0) - 1)


@pytest.mark.parametrize(
    "bkg_spectra, bkg_rates, bkg_rates_min, bkg_rates_max, bkg_rates_var",
    [
        [BKG_SPECTRA1, None, None, None, None],
        [BKG_SPECTRA1, 1000.0, None, None, None],
        [BKG_SPECTRA1, 1000.0, 900.0, 1100.0, 100.0],
        [BKG_SPECTRA2, None, None, None, None],
        [BKG_SPECTRA2, 1000.0, None, None, None],
        [BKG_SPECTRA2, [500.0, 500.0], None, None, None],
        [BKG_SPECTRA2, [500.0, 500.0], None, None, 100.0],
        [BKG_SPECTRA2, [500.0, 500.0], None, None, [100.0, 100.0]],
        [BKG_SPECTRA2, [500.0, 500.0], None, None, [[100.0, 20.0], [20.0, 100.0]]],
        [BKG_SPECTRA2, [500.0, 500.0], 400.0, 600.0, [100.0, 100.0]],
        [BKG_SPECTRA2, [500.0, 500.0], [400.0, 400.0], [600.0, 600.0], [100.0, 100.0]],
    ],
)
@pytest.mark.parametrize(
    "int_time, live_time_min, live_time_max",
    [
        [1.0, None, None],
        [2.0, 1.5, 1.9],
    ],
)
@pytest.mark.parametrize("ts_start", [None, 1.6e9])
def test_random_spec_gen_background(
    bkg_spectra,
    bkg_rates,
    bkg_rates_min,
    bkg_rates_max,
    bkg_rates_var,
    int_time,
    live_time_min,
    live_time_max,
    ts_start,
):
    """Test RandomSpectrumGenerator initialization."""
    gen = RandomSpectrumGenerator(
        bkg_spectra,
        bkg_rates=bkg_rates,
        bkg_rates_min=bkg_rates_min,
        bkg_rates_max=bkg_rates_max,
        bkg_rates_var=bkg_rates_var,
        int_time=int_time,
        live_time_min=live_time_min,
        live_time_max=live_time_max,
        ts_start=ts_start,
    )
    gcr = []
    tss = []
    lts = []
    for n, (spec, spec_id, ts, lt) in enumerate(gen):
        assert len(spec) == len(bkg_spectra[0])
        gcr.append(spec.sum() / lt)
        tss.append(ts)
        lts.append(lt)
        if n == 100:
            break

    # sanity check that the timestamps and live times make sense
    lts = np.array(lts)
    if live_time_min and live_time_max:
        assert ((live_time_min <= lts) & (lts <= live_time_max)).all()
    elif live_time_min:
        assert ((live_time_min <= lts) & (lts <= int_time)).all()
    elif live_time_max:
        assert np.allclose(lts, live_time_max)
    else:
        assert np.allclose(lts, int_time)
    assert np.allclose(np.diff(tss), int_time)


@pytest.mark.parametrize(
    "src_spectra, src_rates, src_rates_var",
    [
        [SRC_SPECTRA0, None, None],
        [SRC_SPECTRA1, 200.0, None],
        [SRC_SPECTRA1, 200.0, 50.0],
        [SRC_SPECTRA2, 200.0, None],
        [SRC_SPECTRA2, [200.0, 300.0], None],
        [SRC_SPECTRA2, [200.0, 300.0], 50.0],
        [SRC_SPECTRA2, [200.0, 300.0], [50.0, 50.0]],
    ],
)
@pytest.mark.parametrize("inject_mode", ["cycle", "interleave", "random"])
@pytest.mark.parametrize("n_bkg", [1, 10])
@pytest.mark.parametrize("n_src", [1, 10])
def test_random_spec_gen_sources(
    src_spectra,
    src_rates,
    src_rates_var,
    inject_mode,
    n_bkg,
    n_src,
):
    """Test RandomSpectrumGenerator initialization."""
    gen = RandomSpectrumGenerator(
        BKG_SPECTRA2,
        src_spectra=src_spectra,
        src_rates=src_rates,
        src_rates_var=src_rates_var,
        inject_mode=inject_mode,
        n_bkg=n_bkg,
        n_src=n_src,
    )
    gcr = []
    tss = []
    lts = []
    for n, (spec, spec_id, ts, lt) in enumerate(gen):
        assert len(spec) == len(BKG_SPECTRA2[0])
        gcr.append(spec.sum() / lt)
        tss.append(ts)
        lts.append(lt)
        if n == 100:
            break


@pytest.mark.parametrize("inject_mode", ["cycle", "interleave", "random"])
def test_random_spec_gen_inject_modes(inject_mode):
    """Plot the indices of the generator with different modes."""
    gen = RandomSpectrumGenerator(
        BKG_SPECTRA2,
        src_spectra=[SRC0, SRC1],
        src_rates=100.0,
        src_rates_var=0.0,
        inject_mode=inject_mode,
        n_bkg=20,
        n_src=5,
        batch_size=200,
    )
    spec_ids = gen.next()[1]
    plt.figure(figsize=(10, 3))
    plt.plot(spec_ids, ".-")
    plt.xlabel("Sample number")
    plt.ylabel("Spectrum ID")
    plt.tight_layout()
    plt.savefig(
        TEST_OUTPUTS_PATH / f"test_spectrum_generator_random_spec_gen_{inject_mode}.png"
    )
    plt.close()


@pytest.mark.parametrize("batch_size", [1, 100, 1000])
def test_random_spec_gen_batch_size(batch_size):
    """Test setting the batch size of the generator."""
    gen = RandomSpectrumGenerator(BKG_SPECTRA2, batch_size=batch_size)
    data = gen.next()
    if batch_size == 1:
        assert len(data[0]) == len(BKG_SPECTRA2[0])
        assert isinstance(data[1], int)
    else:
        assert data[0].shape[0] == batch_size
        assert len(data[1]) == batch_size


@pytest.mark.parametrize("n_total", [1, 100, 1000])
def test_random_spec_gen_n_total(n_total):
    """Test limiting the total number of samples of the generator."""
    gen = RandomSpectrumGenerator(BKG_SPECTRA2, n_total=n_total)
    results = []
    for data in gen:
        results.append(data)
    assert len(results) == n_total


def test_random_spec_gen_plots():
    """Plot the spectra and rates that result from running the generator."""
    gen = RandomSpectrumGenerator(
        BKG_SPECTRA2,
        bkg_rates=[400.0, 200.0],
        bkg_rates_min=[200.0, 20.0],
        bkg_rates_max=[600.0, 400.0],
        bkg_rates_var=[[100.0, -80.0], [-80.0, 100.0]],
        src_spectra=[SRC0, SRC1],
        src_rates=100.0,
        src_rates_var=30.0,
        inject_mode="interleave",
        n_bkg=19,
        n_src=1,
    )

    # plot the background and source spectra
    plt.figure()
    for j in range(gen.bkg_spectra.shape[0]):
        spec = gen.bkg_spectra[j]
        plt.plot(
            *hist_outline((spec, BIN_EDGES)),
            "-",
            lw=2,
            label=f"background spectrum {j}",
        )
    for j in range(gen.src_spectra.shape[0]):
        spec = gen.src_spectra[j]
        plt.plot(
            *hist_outline((spec, BIN_EDGES)), "-", lw=2, label=f"source spectrum {j}"
        )
    plt.legend()
    plt.xlabel("Energy bins (keV)")
    plt.ylabel("Counts per second per bin")
    plt.xlim(BIN_EDGES[0], BIN_EDGES[-1])
    plt.ylim(0)
    plt.tight_layout()
    plt.savefig(TEST_OUTPUTS_PATH / "test_spectrum_generator_plots_spectra.png")
    plt.close()

    # plot every 10 spectra
    gross_rate = []
    for k in range(200):
        data = gen.next()
        gross_rate.append(sum(data[0]) / data[3])
        if (k + 1) % 10 == 0:
            plt.figure()
            spec = data[0]
            plt.plot(
                *hist_outline((spec, BIN_EDGES)),
                "k-",
                lw=2,
                label="spectrum",
            )
            bkg_total = np.zeros_like(spec, dtype=float)
            for j in range(gen.bkg_spectra.shape[0]):
                spec = gen.history_bkg_rates[-1][j] * gen.bkg_spectra[j]
                bkg_total += spec
                plt.plot(
                    *hist_outline((spec, BIN_EDGES)),
                    "-",
                    lw=1,
                    alpha=0.7,
                    label=f"background spectrum {j}",
                )
            plt.plot(
                *hist_outline((bkg_total, BIN_EDGES)),
                "r-",
                lw=2,
                alpha=0.7,
                label="mean total background",
            )
            for j in range(gen.src_spectra.shape[0]):
                spec = gen.history_src_rates[-1][j] * gen.src_spectra[j]
                plt.plot(
                    *hist_outline((spec, BIN_EDGES)),
                    "-",
                    lw=1,
                    alpha=0.7,
                    label=f"source spectrum {j}",
                )
            plt.legend()
            plt.xlabel("Energy bins (keV)")
            plt.ylabel("Counts per second per bin")
            plt.xlim(BIN_EDGES[0], BIN_EDGES[-1])
            plt.ylim(0, 80)
            plt.tight_layout()
            plt.savefig(
                TEST_OUTPUTS_PATH
                / f"test_spectrum_generator_plots_spectrum_{k:03d}.png"
            )
            plt.close()

    # plot the whole history of rates
    plt.figure(figsize=(10, 4))
    plt.plot(gross_rate, "k-", lw=2, label="measured total rate")
    total_rate = np.array(gen.history_bkg_rates).sum(axis=1) + np.array(
        gen.history_src_rates
    ).sum(axis=1)
    plt.plot(total_rate, "r-", lw=2, alpha=0.7, label="simulated total rate")
    for j in range(gen.bkg_spectra.shape[0]):
        rate = np.array(gen.history_bkg_rates)[:, j]
        plt.plot(rate, "-", lw=1, alpha=0.7, label=f"background rate {j}")
    for j in range(gen.src_spectra.shape[0]):
        rate = np.array(gen.history_src_rates)[:, j]
        plt.plot(rate, "-", lw=1, alpha=0.7, label=f"source rate {j}")
    plt.legend()
    plt.xlabel("Sample")
    plt.ylabel("Counts per second")
    plt.xlim(0, len(total_rate))
    plt.ylim(0)
    plt.tight_layout()
    plt.savefig(TEST_OUTPUTS_PATH / "test_spectrum_generator_plots_rates.png")
    plt.close()
