"""Test radai.algorithms.CEW."""

import numpy as np
from radai.algorithms import CEW
from radai.spectrum_generator import RandomSpectrumGenerator


def test_cew():
    """Test the basic functionality of CEW (without training)."""
    # generate background data and train the algorithm
    int_time = 1.0
    spectrum = 15 * np.ones(5)
    np.random.seed(20)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        batch_size=1000,
    )
    alg = CEW(
        int_time=int_time,
        w_s=[0, 1, 1, 1, 0],
        w_b=[1, 0, 0, 0, 1],
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_cew_fit_known():
    """Test CEW including running fit with a known source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with known spectra
    alg = CEW(
        int_time=int_time,
        w_s=[],
        w_b=[],
    )
    alg.fit_known_sources(
        train_spectra,
        train_ids,
        spec_src,
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_cew_fit_nmf():
    """Test CEW including running fit_nmf with an unknown source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with unknown spectra
    alg = CEW(
        int_time=int_time,
        w_s=[],
        w_b=[],
    )
    alg.fit_nmf(
        train_spectra,
        train_ids,
        src_ids=[1],
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_cew_fit():
    """Test CEW including running fit with an unknown source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with unknown spectra
    alg = CEW(
        int_time=int_time,
        w_s=[],
        w_b=[],
    )
    alg.fit(
        train_spectra,
        train_ids,
        src_ids=[1],
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
