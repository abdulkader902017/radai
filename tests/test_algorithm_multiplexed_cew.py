"""Test radai.algorithms.CEW."""

import numpy as np
from radai.algorithms import CEW, MultiplexedCEW
from radai.spectrum_generator import RandomSpectrumGenerator


def test_multiplexed_cew_init():
    """Test the basic functionality of MultiplexedCEW (without training)."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(5)
    np.random.seed(20)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        batch_size=1000,
    )

    # create sub-algorithms
    alg1 = CEW(
        int_time=int_time,
        w_s=[1, 1, 1, 0, 0],
        w_b=[0, 0, 0, 1, 1],
    )
    alg2 = CEW(
        int_time=int_time,
        w_s=[0, 1, 1, 1, 0],
        w_b=[1, 0, 0, 0, 1],
    )
    alg3 = CEW(
        int_time=int_time,
        w_s=[0, 0, 1, 1, 1],
        w_b=[1, 1, 0, 0, 0],
    )

    # instantiate multiplexed algorithm
    alg = MultiplexedCEW(
        int_time=int_time,
        alg1=alg1.to_dict(),
        alg2=alg2.to_dict(),
        alg3=alg3.to_dict(),
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_multiplexed_cew_fit():
    """Test MultiplexedCEW including running fit with an unknown source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with unknown spectra
    alg = MultiplexedCEW(int_time=int_time)
    alg.fit(train_spectra, train_ids, src_ids=[1])

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_multiplexed_cew_fit_known():
    """Test MultiplexedCEW including running fit with a known source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with known spectra
    alg = MultiplexedCEW(int_time=int_time)
    alg.fit_known_sources(train_spectra, train_ids, [spec_src])
    assert len(alg.sub_alg) == 1

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    results = alg.analyze(test_spectrum, test_ts, test_lt)
    assert len(results) == 1

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    batch_results = alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
    assert len(batch_results) == gen.batch_size
    assert len(batch_results[0]) == 1


def test_multiplexed_cew_fit_nmf():
    """Test MultiplexedCEW including running fit_nmf with an unknown source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # find source spectra using NMF
    alg = MultiplexedCEW(int_time=int_time)
    alg.fit_nmf(train_spectra, train_ids, src_ids=[1])

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
