import tempfile
from pathlib import Path
import pytest
import numpy as np
import radai
from common import FakeAlgo


@pytest.mark.parametrize(
    "extension",
    (
        ".tar",
        ".tgz",
        ".tar.gz",
        ".tar.bz2",
        ".tar.xz",
        ".yaml",
        ".json",
        ".pkl",
    ),
)
def test_io(extension):
    algo = FakeAlgo(1, np.arange(10), [{"a": 1, "b": True, "c": list(range(10))}])
    assert algo.is_ready()
    path_no_ext = Path(tempfile.gettempdir()) / "fake_algo"
    this_path = path_no_ext.with_suffix(extension)
    print(this_path)
    algo.write(this_path)
    algo_from_disk = radai.io.read(this_path)
    assert algo.param0 == algo_from_disk.param0
    assert np.allclose(algo.data0, algo_from_disk.data0)
    assert algo.data1 == algo_from_disk.data1


def test_analyze_when_not_ready():
    algo = FakeAlgo(1, np.arange(2), list(range(10)), ready=False)
    assert not algo.is_ready()
    results = algo.analyze(np.arange(10), 1.0, 1.0)
    assert len(results) == 0
