"""Test radai.algorithms.KSigma"""

import numpy as np
from radai.algorithms import KSigma
from radai.spectrum_generator import RandomSpectrumGenerator


def test_gross_counts():
    """Test the basic functionality of KSigma."""
    int_time = 1.0
    alg = KSigma(
        int_time=int_time, filter={"classname": "StaticFilter", "mean": 1500, "cov": 30}
    )

    # generate background data and train the algorithm
    spectrum = 30 * np.ones(100)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        bkg_rates=[1500.0],
        batch_size=1000,
    )
    train_spectra, _, _, train_lts = gen.next()
    alg.fit(train_spectra, live_times=train_lts)

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, _, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, _, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
